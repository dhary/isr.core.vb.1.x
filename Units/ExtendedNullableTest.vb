﻿Imports System
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core
'''<summary>
'''This is a test class for ExtendedNullableTest and is intended
'''to contain all ExtendedNullableTest Unit Tests
'''</summary>
<TestClass()> _
<CLSCompliant(False)> _
Public Class ExtendedNullableTest

    Private testContextInstance As TestContext

  '''<summary>
  '''Gets or sets the test context which provides
  '''information about and functionality for the current test run.
  '''</summary>
  Public Property TestContext() As TestContext
    Get
      Return testContextInstance
    End Get
    Set(ByVal value As TestContext)
      testContextInstance = value
    End Set
  End Property

#Region "Additional test attributes"
  '
  'You can use the following additional attributes as you write your tests:
  '
  'Use ClassInitialize to run code before running the first test in the class
  '<ClassInitialize()>  _
  'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
  'End Sub
  '
  'Use ClassCleanup to run code after all tests in a class have run
  '<ClassCleanup()>  _
  'Public Shared Sub MyClassCleanup()
  'End Sub
  '
  'Use TestInitialize to run code before running each test
  '<TestInitialize()>  _
  'Public Sub MyTestInitialize()
  'End Sub
  '
  'Use TestCleanup to run code after each test has run
  '<TestCleanup()>  _
  'Public Sub MyTestCleanup()
  'End Sub
  '
#End Region

  <TestMethod()> _
  Public Sub ValidateFieldValueTest()
    Dim target As ExtendedNullable(Of Double) = New ExtendedNullable(Of Double)
    Dim expected As Boolean = True
    Dim actual As Boolean
    target.LowLimit = -1
    target.HighLimit = 1
    target.MinLength = 0
    target.MaxLength = 10
    target.Value = 0.1
    actual = target.ValidateFieldValue
    Assert.AreEqual(expected, actual)
  End Sub

  <TestMethod()> _
  Public Sub TrySetValueFromStringTest()
    Dim target As ExtendedNullable(Of Double) = New ExtendedNullable(Of Double)
    Dim value As String = "0.01"
    Dim expected As Boolean = True
    Dim actual As Boolean
    actual = target.TrySetValue(value)
    Assert.AreEqual(expected, actual)
  End Sub

  ''' <summary>
  ''' This test fails.
  ''' </summary>
  ''' <remarks></remarks>
  <TestMethod()> _
  Public Sub TrySetValueTestAndFail()
    Dim target As ExtendedNullable(Of Double) = New ExtendedNullable(Of Double)
    Dim value As String = "0.01"
    Dim expected As Double = CDbl(value)
    Dim outcome As Boolean = target.TrySetValue(Of String)(value)
    Dim actual As Double = target.Value
    Assert.AreNotEqual(expected, actual, 0.00001, "Details={0}", target.Details)
  End Sub

  <TestMethod()> _
  Public Sub TrySetValueTest()
    Dim target As ExtendedNullable(Of Double) = New ExtendedNullable(Of Double)
    Dim value As Double = 0.1!
    Dim expected As Boolean = True
    Dim actual As Boolean
    actual = target.TrySetValue(value)
    Assert.AreEqual(expected, actual)
    Assert.AreEqual(value, target.Value, 0.00001, "actual {0} <> expected {1}", value, target.Value)
  End Sub

  <TestMethod()> _
  Public Sub TrySetValueFromDecimalTest()
    Dim target As ExtendedNullable(Of Double) = New ExtendedNullable(Of Double)
    Dim value As Decimal = CDec(0.01)
    Dim expected As Double = CDbl(value)
    Dim outcome As Boolean = target.TrySetValue(Of Decimal)(value)
    Dim actual As Double = target.Value
    Assert.AreEqual(expected, actual, 0.00001, "Details={0}", target.Details)
  End Sub

End Class
