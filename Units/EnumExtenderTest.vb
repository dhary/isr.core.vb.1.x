﻿Imports System
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core

'''<summary>
'''This is a test class for EfficientEnumTest and is intended
'''to contain all EfficientEnumTest Unit Tests
'''</summary>
<TestClass()> _
Public Class EnumExtenderTest


  Private testContextInstance As TestContext

  '''<summary>
  '''Gets or sets the test context which provides
  '''information about and functionality for the current test run.
  '''</summary>
  Public Property TestContext() As TestContext
    Get
      Return testContextInstance
    End Get
    Set(ByVal value As TestContext)
      testContextInstance = Value
    End Set
  End Property

#Region "Additional test attributes"
  '
  'You can use the following additional attributes as you write your tests:

  Private Shared colorEnum As EnumExtender(Of Color)
  'Use ClassInitialize to run code before running the first test in the class
  <ClassInitialize()> _
  Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    colorEnum = New EnumExtender(Of Color)()
  End Sub

  'Use ClassCleanup to run code after all tests in a class have run
  '<ClassCleanup()>  _
  'Public Shared Sub MyClassCleanup()
  'End Sub
  '
  'Use TestInitialize to run code before running each test
  '<TestInitialize()>  _
  'Public Sub MyTestInitialize()
  'End Sub
  '
  'Use TestCleanup to run code after each test has run
  '<TestCleanup()>  _
  'Public Sub MyTestCleanup()
  'End Sub
  '
#End Region

  Private Enum Color
    <ComponentModel.Description("While Color")> White
    <ComponentModel.Description("Black Color")> Black
    <ComponentModel.Description("Red Color")> Red
    <ComponentModel.Description("Yellow Color")> Yellow
    <ComponentModel.Description("Blue Color")> Blue
    <ComponentModel.Description("Green Color")> Green
    <ComponentModel.Description("Cyan Color")> Cyan
    <ComponentModel.Description("Magenta Color")> Magenta
    <ComponentModel.Description("Pink Color")> Pink
    <ComponentModel.Description("Purple Color")> Purple
    <ComponentModel.Description("Orange Color")> Orange
    <ComponentModel.Description("Brown Color")> Brown
  End Enum

  Private Shared _enumStrings As String() = New String() {"White", "Black", "Red", "Yellow", "Blue", "Green", "Cyan", "Magenta", "Pink", "Purple", "Orange", "Brown"}

  Private Const _iterations As Integer = 100000

  Shared Sub Main(ByVal args As String())
    Dim randomNumber As Random = New Random()
    Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{Built-in Enum class}")
      For i As Integer = 0 To _iterations - 1
        Dim index As Integer = randomNumber.Next(0, 11)
        Dim c1 As Color = CType(System.Enum.ToObject(GetType(Color), index), Color)
        Dim c2 As Color = CType(System.Enum.Parse(GetType(Color), _enumStrings(index)), Color)
      Next i
    End Using

    ' Verify initialization of the data out of the comparative measurement.
    ' As you can see, this initialization is the gain for the later efficiency.
    Dim colorEnum As New EnumExtender(Of Color)
    Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{StrongQuickEnum<Color> class}")
      For i As Integer = 0 To _iterations - 1
        Dim index As Integer = randomNumber.Next(0, 11)
        Dim c1 As Color = colorEnum.ToObject(index)
        Dim c2 As Color = colorEnum.Parse(_enumStrings(index))
      Next i
    End Using
    Console.ReadLine()
  End Sub

  Friend Class PerformanceMonitor
    Implements IDisposable
    Private _timestarted As Long
    Private _name As String

    Friend Sub New(ByVal name As String)
      _name = name
      _timestarted = DateTime.Now.Ticks
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
      Console.WriteLine("Operation " & _name & ":" & Constants.vbTab + Constants.vbTab + (DateTime.Now.Ticks - _timestarted).ToString())
    End Sub
  End Class

  <TestMethod()> _
  Public Sub ToObjectTest1()

    Dim value As Integer = CInt(Color.Cyan)
    Dim expected As Color = Color.Cyan
    Dim actual As Color
    actual = colorEnum.ToObject(value)
    Assert.AreEqual(expected, actual)

  End Sub

  <TestMethod()> _
  Public Sub ToObjectTest2()
    Dim value As Integer = CInt(Color.Cyan)
    Dim expected As Color = Color.Cyan
    Dim actual As Color
    actual = colorEnum.ToObject(value)
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub ParseTest1()
    Dim value As String = String.Empty ' TODO: Initialize to an appropriate value
    Dim ignoreCase As Boolean = False ' TODO: Initialize to an appropriate value
    Dim expected As Color = CType(Nothing, Color) ' TODO: Initialize to an appropriate value
    Dim actual As Color
    actual = colorEnum.Parse(value, ignoreCase)
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  '''<summary>
  '''A test for Parse
  '''</summary>
  Public Sub ParseTestHelper(Of T)()
    Dim value As String = String.Empty ' TODO: Initialize to an appropriate value
    Dim expected As Color = Color.Cyan
    Dim actual As Color
    actual = colorEnum.Parse(value)
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub ParseTest()
    ParseTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for IsDefined
  '''</summary>
  Public Sub IsDefinedTestHelper(Of T)()
    Dim value As Object = Nothing ' TODO: Initialize to an appropriate value
    Dim expected As Boolean = False ' TODO: Initialize to an appropriate value
    Dim actual As Boolean
    actual = colorEnum.IsDefined(value)
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub IsDefinedTest()
    IsDefinedTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for GetValues
  '''</summary>
  Public Sub GetValuesTestHelper(Of T)()
    Dim expected As Array = Nothing ' TODO: Initialize to an appropriate value
    Dim actual As Array
    actual = colorEnum.GetValues
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub GetValuesTest()
    GetValuesTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for GetUnderlyingType
  '''</summary>
  Public Sub GetUnderlyingTypeTestHelper(Of T)()
    Dim expected As Type = Nothing ' TODO: Initialize to an appropriate value
    Dim actual As Type
    actual = colorEnum.GetUnderlyingType
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub GetUnderlyingTypeTest()
    GetUnderlyingTypeTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for GetNames
  '''</summary>
  Public Sub GetNamesTestHelper(Of T)()
    Dim expected() As String = Nothing ' TODO: Initialize to an appropriate value
    Dim actual() As String
    actual = colorEnum.GetNames
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub GetNamesTest()
    GetNamesTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for GetName
  '''</summary>
  Public Sub GetNameTestHelper(Of T)()
    Dim value As Object = Nothing ' TODO: Initialize to an appropriate value
    Dim expected As String = String.Empty ' TODO: Initialize to an appropriate value
    Dim actual As String
    actual = colorEnum.GetName(value)
    Assert.AreEqual(expected, actual)
    Assert.Inconclusive("Verify the correctness of this test method.")
  End Sub

  <TestMethod()> _
  Public Sub GetNameTest()
    GetNameTestHelper(Of GenericParameterHelper)()
  End Sub

  '''<summary>
  '''A test for EfficientEnum`1 Constructor
  '''</summary>
  Public Sub EfficientEnumConstructorTestHelper(Of T)()
    Dim target As EnumExtender(Of T) = New EnumExtender(Of T)
    Assert.Inconclusive("TODO: Implement code to verify target")
  End Sub

  <TestMethod()> _
  Public Sub EfficientEnumConstructorTest()
    EfficientEnumConstructorTestHelper(Of GenericParameterHelper)()
  End Sub
End Class
