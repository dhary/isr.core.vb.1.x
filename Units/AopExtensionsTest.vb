﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.AOP

Public Class ExcensionTestTarget

    Public Sub New()
        MyBase.New()
        newPropertyValue2 = ""
    End Sub

    Private newPropertyValue As String
    <ComponentModel.DefaultValue("New Property")> _
    Public Property NewProperty() As String
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As String)
            newPropertyValue = value
        End Set
    End Property

    Private newPropertyValue1 As String
    <ComponentModel.DefaultValue("New Property 1")> _
    Public Property NewProperty1() As String
        Get
            Return newPropertyValue1
        End Get
        Set(ByVal value As String)
            newPropertyValue1 = value
        End Set
    End Property

    Private newPropertyValue2 As String
    Public Property NewProperty2() As String
        Get
            Return newPropertyValue2
        End Get
        Set(ByVal value As String)
            newPropertyValue2 = value
        End Set
    End Property

End Class

Public Class SelectInitTestTarget

    Public Sub New()
        MyBase.New()
        newPropertyValue2 = ""
        Me.ApplyDefaultValues()
    End Sub

    Private newPropertyValue As String
    <ComponentModel.DefaultValue("New Property")> _
    Public Property NewProperty() As String
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As String)
            newPropertyValue = value
        End Set
    End Property

    Private newPropertyValue1 As String
    <ComponentModel.DefaultValue("New Property 1")> _
    Public Property NewProperty1() As String
        Get
            Return newPropertyValue1
        End Get
        Set(ByVal value As String)
            newPropertyValue1 = value
        End Set
    End Property

    Private newPropertyValue2 As String
    Public Property NewProperty2() As String
        Get
            Return newPropertyValue2
        End Get
        Set(ByVal value As String)
            newPropertyValue2 = value
        End Set
    End Property

End Class

'''<summary>
'''This is a test class for ExtensionsTest and is intended
'''to contain all ExtensionsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class AopExtensionsTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for ApplyDefaultValues
    '''</summary>
    <TestMethod()> _
    Public Sub ApplyDefaultValuesTest()
        Dim _this As ExcensionTestTarget = New ExcensionTestTarget
        Extensions.ApplyDefaultValues(_this)
        Assert.AreEqual(_this.NewProperty, "New Property")
        Assert.AreEqual(_this.NewProperty1, "New Property 1")
        Assert.AreEqual(_this.NewProperty2, "")
    End Sub

    '''<summary>
    '''A test for ResetDefaultValues
    '''</summary>
    <TestMethod()> _
    Public Sub ResetDefaultValuesTest()
        Dim _this As ExcensionTestTarget = New ExcensionTestTarget
        Extensions.ApplyDefaultValues(_this)
        Assert.AreEqual(_this.NewProperty, "New Property")
        Assert.AreEqual(_this.NewProperty1, "New Property 1")
        Assert.AreEqual(_this.NewProperty2, "")
        _this.NewProperty = "0"
        _this.NewProperty1 = "1"
        _this.NewProperty2 = "2"
        Extensions.ResetDefaultValues(_this)
        Assert.AreEqual(_this.NewProperty, "New Property")
        Assert.AreEqual(_this.NewProperty1, "New Property 1")
        Assert.AreEqual(_this.NewProperty2, "2")
    End Sub

    '''<summary>
    '''A test for ApplyDefaultValues with self initialization.
    '''</summary>
    <TestMethod()> _
    Public Sub ApplyDefaultValuesTestSelectInit()
        Dim _this As SelectInitTestTarget = New SelectInitTestTarget
        Assert.AreEqual(_this.NewProperty, "New Property")
        Assert.AreEqual(_this.NewProperty1, "New Property 1")
        Assert.AreEqual(_this.NewProperty2, "")
        _this.NewProperty = "0"
        _this.NewProperty1 = "1"
        _this.NewProperty2 = "2"
        Extensions.ResetDefaultValues(_this)
        Assert.AreEqual(_this.NewProperty, "New Property")
        Assert.AreEqual(_this.NewProperty1, "New Property 1")
        Assert.AreEqual(_this.NewProperty2, "2")
    End Sub

End Class
