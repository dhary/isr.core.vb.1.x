Friend Class MyEventArgs
  Inherits EventArgs

  Public Sub New(ByVal eventData As String)
    Me.EventData = eventData
  End Sub

  Private _eventData As String
  Public Property EventData() As String
    Get
      Return _eventData
    End Get
    Private Set(ByVal value As String)
      _eventData = value
    End Set
  End Property

  Private _returnData As String
  Public Property ReturnData() As String
    Get
      Return _returnData
    End Get
    Set(ByVal value As String)
      _returnData = value
    End Set
  End Property

End Class
