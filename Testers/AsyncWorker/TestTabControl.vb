﻿Imports System.Windows.Forms
Imports System.Text

''' <summary>
''' sets the Tabpages Text to the Type-name of the at last added Control
''' </summary>
Public Class TestTabControl
   Inherits TabControl

   Protected Overrides Sub OnControlAdded(ByVal e As ControlEventArgs)
      MyBase.OnControlAdded(e)
      AddHandler e.Control.ControlAdded, AddressOf tp_ControlAdded
      AddHandler e.Control.ControlRemoved, AddressOf tp_ControlRemoved
   End Sub

   Private Sub tp_ControlRemoved(ByVal sender As Object, ByVal e As ControlEventArgs)
      Dim tp = DirectCast(sender, TabPage)
      If tp.Controls.Count = 0 Then tp.Text = tp.Name
   End Sub

   Private Sub tp_ControlAdded(ByVal sender As Object, ByVal e As ControlEventArgs)
      Dim tp = DirectCast(sender, TabPage)
      Dim s = e.Control.GetType().Name
      'skip (lowchar-) prefixes
      For i = 0 To s.Length - 1
         If Char.IsUpper(s(i)) Then
            tp.Text = s.Substring(i)
            Return
         End If
      Next
   End Sub

End Class
