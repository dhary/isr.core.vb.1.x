﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uclDataBoundWorker
   Inherits System.Windows.Forms.UserControl

   'UserControl overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
      Me.label1 = New System.Windows.Forms.Label
      Me.progressBar1 = New System.Windows.Forms.ProgressBar
      Me.btCancel = New System.Windows.Forms.Button
      Me.SuspendLayout()
      '
      'label1
      '
      Me.label1.AutoSize = True
      Me.label1.BackColor = System.Drawing.SystemColors.Control
      Me.label1.Location = New System.Drawing.Point(6, 9)
      Me.label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
      Me.label1.Name = "label1"
      Me.label1.Size = New System.Drawing.Size(97, 16)
      Me.label1.TabIndex = 2
      Me.label1.Text = "Click in here!"
      '
      'progressBar1
      '
      Me.progressBar1.BackColor = System.Drawing.SystemColors.Control
      Me.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom
      Me.progressBar1.Location = New System.Drawing.Point(0, 73)
      Me.progressBar1.Margin = New System.Windows.Forms.Padding(4)
      Me.progressBar1.Name = "progressBar1"
      Me.progressBar1.Size = New System.Drawing.Size(199, 28)
      Me.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
      Me.progressBar1.TabIndex = 8
      '
      'btCancel
      '
      Me.btCancel.BackColor = System.Drawing.SystemColors.Control
      Me.btCancel.Location = New System.Drawing.Point(3, 28)
      Me.btCancel.Name = "btCancel"
      Me.btCancel.Size = New System.Drawing.Size(75, 23)
      Me.btCancel.TabIndex = 10
      Me.btCancel.Text = "Cancel"
      Me.btCancel.UseVisualStyleBackColor = False
      '
      'uclDataBoundWorker
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 16.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.BackColor = System.Drawing.Color.OrangeRed
      Me.Controls.Add(Me.btCancel)
      Me.Controls.Add(Me.progressBar1)
      Me.Controls.Add(Me.label1)
      Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Margin = New System.Windows.Forms.Padding(4)
      Me.Name = "uclDataBoundWorker"
      Me.Size = New System.Drawing.Size(199, 101)
      Me.ResumeLayout(False)
      Me.PerformLayout()

   End Sub
   Private WithEvents label1 As System.Windows.Forms.Label
   Private WithEvents progressBar1 As System.Windows.Forms.ProgressBar
   Friend WithEvents btCancel As System.Windows.Forms.Button

End Class
