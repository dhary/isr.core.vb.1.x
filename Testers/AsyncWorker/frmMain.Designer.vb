<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
   Inherits System.Windows.Forms.Form

   'Form overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
      Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
      Me.PictureBox5 = New System.Windows.Forms.PictureBox
      Me.PictureBox6 = New System.Windows.Forms.PictureBox
      Me.PictureBox4 = New System.Windows.Forms.PictureBox
      Me.TestTabControl1 = New AsyncWorkerVB.TestTabControl
      Me.TabPage1 = New System.Windows.Forms.TabPage
      Me.UclPureGui1 = New AsyncWorkerVB.uclPureGui
      Me.TabPage2 = New System.Windows.Forms.TabPage
      Me.UclStillBlocking1 = New AsyncWorkerVB.uclStillBlocking
      Me.TabPage3 = New System.Windows.Forms.TabPage
      Me.UclNonBlocking1 = New AsyncWorkerVB.uclNonBlocking
      Me.TabPage4 = New System.Windows.Forms.TabPage
      Me.UclBgwSimple1 = New AsyncWorkerVB.uclBgwSimple
      Me.TabPage5 = New System.Windows.Forms.TabPage
      Me.UclBgwPrgbar1 = New AsyncWorkerVB.uclBgwPrgbar
      Me.TabPage6 = New System.Windows.Forms.TabPage
      Me.UclAsyncWorker1 = New AsyncWorkerVB.uclAsyncWorker
      Me.TabPage7 = New System.Windows.Forms.TabPage
      Me.UclDataBoundWorker1 = New AsyncWorkerVB.uclDataBoundWorker
      Me.TabPage8 = New System.Windows.Forms.TabPage
      Me.UclCountDown1 = New AsyncWorkerVB.uclCountDown
      Me.TabPage9 = New System.Windows.Forms.TabPage
      Me.UclExtensions1 = New AsyncWorkerVB.uclExtensions
      Me.TabPage10 = New System.Windows.Forms.TabPage
      Me.UclExceptions1 = New AsyncWorkerVB.uclExceptions
      Me.TableLayoutPanel1.SuspendLayout()
      CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
      CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.TestTabControl1.SuspendLayout()
      Me.TabPage1.SuspendLayout()
      Me.TabPage2.SuspendLayout()
      Me.TabPage3.SuspendLayout()
      Me.TabPage4.SuspendLayout()
      Me.TabPage5.SuspendLayout()
      Me.TabPage6.SuspendLayout()
      Me.TabPage7.SuspendLayout()
      Me.TabPage8.SuspendLayout()
      Me.TabPage9.SuspendLayout()
      Me.TabPage10.SuspendLayout()
      Me.SuspendLayout()
      '
      'TableLayoutPanel1
      '
      Me.TableLayoutPanel1.ColumnCount = 1
      Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
      Me.TableLayoutPanel1.Controls.Add(Me.PictureBox5, 0, 0)
      Me.TableLayoutPanel1.Controls.Add(Me.PictureBox6, 0, 1)
      Me.TableLayoutPanel1.Controls.Add(Me.PictureBox4, 0, 2)
      Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left
      Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
      Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
      Me.TableLayoutPanel1.RowCount = 3
      Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
      Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
      Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
      Me.TableLayoutPanel1.Size = New System.Drawing.Size(60, 379)
      Me.TableLayoutPanel1.TabIndex = 14
      '
      'PictureBox5
      '
      Me.PictureBox5.Dock = System.Windows.Forms.DockStyle.Fill
      Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
      Me.PictureBox5.Location = New System.Drawing.Point(3, 3)
      Me.PictureBox5.Name = "PictureBox5"
      Me.PictureBox5.Size = New System.Drawing.Size(54, 120)
      Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
      Me.PictureBox5.TabIndex = 10
      Me.PictureBox5.TabStop = False
      '
      'PictureBox6
      '
      Me.PictureBox6.Dock = System.Windows.Forms.DockStyle.Fill
      Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
      Me.PictureBox6.Location = New System.Drawing.Point(3, 129)
      Me.PictureBox6.Name = "PictureBox6"
      Me.PictureBox6.Size = New System.Drawing.Size(54, 120)
      Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
      Me.PictureBox6.TabIndex = 8
      Me.PictureBox6.TabStop = False
      '
      'PictureBox4
      '
      Me.PictureBox4.Dock = System.Windows.Forms.DockStyle.Fill
      Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
      Me.PictureBox4.Location = New System.Drawing.Point(3, 255)
      Me.PictureBox4.Name = "PictureBox4"
      Me.PictureBox4.Size = New System.Drawing.Size(54, 121)
      Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
      Me.PictureBox4.TabIndex = 9
      Me.PictureBox4.TabStop = False
      '
      'TestTabControl1
      '
      Me.TestTabControl1.Controls.Add(Me.TabPage1)
      Me.TestTabControl1.Controls.Add(Me.TabPage2)
      Me.TestTabControl1.Controls.Add(Me.TabPage3)
      Me.TestTabControl1.Controls.Add(Me.TabPage4)
      Me.TestTabControl1.Controls.Add(Me.TabPage5)
      Me.TestTabControl1.Controls.Add(Me.TabPage6)
      Me.TestTabControl1.Controls.Add(Me.TabPage7)
      Me.TestTabControl1.Controls.Add(Me.TabPage8)
      Me.TestTabControl1.Controls.Add(Me.TabPage9)
      Me.TestTabControl1.Controls.Add(Me.TabPage10)
      Me.TestTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.TestTabControl1.Location = New System.Drawing.Point(60, 0)
      Me.TestTabControl1.Name = "TestTabControl1"
      Me.TestTabControl1.SelectedIndex = 0
      Me.TestTabControl1.Size = New System.Drawing.Size(737, 379)
      Me.TestTabControl1.TabIndex = 0
      '
      'TabPage1
      '
      Me.TabPage1.Controls.Add(Me.UclPureGui1)
      Me.TabPage1.Location = New System.Drawing.Point(4, 22)
      Me.TabPage1.Name = "TabPage1"
      Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage1.Size = New System.Drawing.Size(729, 353)
      Me.TabPage1.TabIndex = 0
      Me.TabPage1.Text = "PureGui"
      Me.TabPage1.UseVisualStyleBackColor = True
      '
      'UclPureGui1
      '
      Me.UclPureGui1.BackColor = System.Drawing.Color.OrangeRed
      Me.UclPureGui1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclPureGui1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclPureGui1.Location = New System.Drawing.Point(3, 3)
      Me.UclPureGui1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclPureGui1.Name = "UclPureGui1"
      Me.UclPureGui1.Size = New System.Drawing.Size(723, 347)
      Me.UclPureGui1.TabIndex = 0
      '
      'TabPage2
      '
      Me.TabPage2.Controls.Add(Me.UclStillBlocking1)
      Me.TabPage2.Location = New System.Drawing.Point(4, 22)
      Me.TabPage2.Name = "TabPage2"
      Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage2.Size = New System.Drawing.Size(655, 353)
      Me.TabPage2.TabIndex = 1
      Me.TabPage2.Text = "StillBlocking"
      Me.TabPage2.UseVisualStyleBackColor = True
      '
      'UclStillBlocking1
      '
      Me.UclStillBlocking1.BackColor = System.Drawing.Color.SandyBrown
      Me.UclStillBlocking1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclStillBlocking1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclStillBlocking1.Location = New System.Drawing.Point(3, 3)
      Me.UclStillBlocking1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclStillBlocking1.Name = "UclStillBlocking1"
      Me.UclStillBlocking1.Size = New System.Drawing.Size(649, 347)
      Me.UclStillBlocking1.TabIndex = 0
      '
      'TabPage3
      '
      Me.TabPage3.Controls.Add(Me.UclNonBlocking1)
      Me.TabPage3.Location = New System.Drawing.Point(4, 22)
      Me.TabPage3.Name = "TabPage3"
      Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage3.Size = New System.Drawing.Size(655, 353)
      Me.TabPage3.TabIndex = 2
      Me.TabPage3.Text = "NonBlocking"
      Me.TabPage3.UseVisualStyleBackColor = True
      '
      'UclNonBlocking1
      '
      Me.UclNonBlocking1.BackColor = System.Drawing.Color.Bisque
      Me.UclNonBlocking1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclNonBlocking1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclNonBlocking1.Location = New System.Drawing.Point(3, 3)
      Me.UclNonBlocking1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclNonBlocking1.Name = "UclNonBlocking1"
      Me.UclNonBlocking1.Size = New System.Drawing.Size(649, 347)
      Me.UclNonBlocking1.TabIndex = 0
      '
      'TabPage4
      '
      Me.TabPage4.Controls.Add(Me.UclBgwSimple1)
      Me.TabPage4.Location = New System.Drawing.Point(4, 22)
      Me.TabPage4.Name = "TabPage4"
      Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage4.Size = New System.Drawing.Size(655, 353)
      Me.TabPage4.TabIndex = 3
      Me.TabPage4.Text = "BgwSimple"
      Me.TabPage4.UseVisualStyleBackColor = True
      '
      'UclBgwSimple1
      '
      Me.UclBgwSimple1.BackColor = System.Drawing.Color.OldLace
      Me.UclBgwSimple1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclBgwSimple1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclBgwSimple1.Location = New System.Drawing.Point(3, 3)
      Me.UclBgwSimple1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclBgwSimple1.Name = "UclBgwSimple1"
      Me.UclBgwSimple1.Size = New System.Drawing.Size(649, 347)
      Me.UclBgwSimple1.TabIndex = 0
      '
      'TabPage5
      '
      Me.TabPage5.Controls.Add(Me.UclBgwPrgbar1)
      Me.TabPage5.Location = New System.Drawing.Point(4, 22)
      Me.TabPage5.Name = "TabPage5"
      Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage5.Size = New System.Drawing.Size(655, 353)
      Me.TabPage5.TabIndex = 4
      Me.TabPage5.Text = "BgwPrgbar"
      Me.TabPage5.UseVisualStyleBackColor = True
      '
      'UclBgwPrgbar1
      '
      Me.UclBgwPrgbar1.BackColor = System.Drawing.Color.Gold
      Me.UclBgwPrgbar1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclBgwPrgbar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclBgwPrgbar1.Location = New System.Drawing.Point(3, 3)
      Me.UclBgwPrgbar1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclBgwPrgbar1.Name = "UclBgwPrgbar1"
      Me.UclBgwPrgbar1.Size = New System.Drawing.Size(649, 347)
      Me.UclBgwPrgbar1.TabIndex = 0
      '
      'TabPage6
      '
      Me.TabPage6.Controls.Add(Me.UclAsyncWorker1)
      Me.TabPage6.Location = New System.Drawing.Point(4, 22)
      Me.TabPage6.Name = "TabPage6"
      Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage6.Size = New System.Drawing.Size(655, 353)
      Me.TabPage6.TabIndex = 5
      Me.TabPage6.Text = "AsyncWorker"
      Me.TabPage6.UseVisualStyleBackColor = True
      '
      'UclAsyncWorker1
      '
      Me.UclAsyncWorker1.BackColor = System.Drawing.Color.Khaki
      Me.UclAsyncWorker1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclAsyncWorker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclAsyncWorker1.Location = New System.Drawing.Point(3, 3)
      Me.UclAsyncWorker1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclAsyncWorker1.Name = "UclAsyncWorker1"
      Me.UclAsyncWorker1.Size = New System.Drawing.Size(649, 347)
      Me.UclAsyncWorker1.TabIndex = 0
      '
      'TabPage7
      '
      Me.TabPage7.Controls.Add(Me.UclDataBoundWorker1)
      Me.TabPage7.Location = New System.Drawing.Point(4, 22)
      Me.TabPage7.Name = "TabPage7"
      Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage7.Size = New System.Drawing.Size(655, 353)
      Me.TabPage7.TabIndex = 6
      Me.TabPage7.Text = "DataBoundWorker"
      Me.TabPage7.UseVisualStyleBackColor = True
      '
      'UclDataBoundWorker1
      '
      Me.UclDataBoundWorker1.BackColor = System.Drawing.Color.LemonChiffon
      Me.UclDataBoundWorker1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclDataBoundWorker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclDataBoundWorker1.Location = New System.Drawing.Point(3, 3)
      Me.UclDataBoundWorker1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclDataBoundWorker1.Name = "UclDataBoundWorker1"
      Me.UclDataBoundWorker1.Size = New System.Drawing.Size(649, 347)
      Me.UclDataBoundWorker1.TabIndex = 0
      '
      'TabPage8
      '
      Me.TabPage8.Controls.Add(Me.UclCountDown1)
      Me.TabPage8.Location = New System.Drawing.Point(4, 22)
      Me.TabPage8.Name = "TabPage8"
      Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage8.Size = New System.Drawing.Size(655, 353)
      Me.TabPage8.TabIndex = 7
      Me.TabPage8.Text = "CountDown"
      Me.TabPage8.UseVisualStyleBackColor = True
      '
      'UclCountDown1
      '
      Me.UclCountDown1.BackColor = System.Drawing.Color.MistyRose
      Me.UclCountDown1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclCountDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclCountDown1.Location = New System.Drawing.Point(3, 3)
      Me.UclCountDown1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclCountDown1.Name = "UclCountDown1"
      Me.UclCountDown1.Size = New System.Drawing.Size(649, 347)
      Me.UclCountDown1.TabIndex = 0
      '
      'TabPage9
      '
      Me.TabPage9.Controls.Add(Me.UclExtensions1)
      Me.TabPage9.Location = New System.Drawing.Point(4, 22)
      Me.TabPage9.Name = "TabPage9"
      Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage9.Size = New System.Drawing.Size(655, 353)
      Me.TabPage9.TabIndex = 8
      Me.TabPage9.Text = "Extensions"
      Me.TabPage9.UseVisualStyleBackColor = True
      '
      'UclExtensions1
      '
      Me.UclExtensions1.BackColor = System.Drawing.Color.PaleGreen
      Me.UclExtensions1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclExtensions1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.UclExtensions1.Location = New System.Drawing.Point(3, 3)
      Me.UclExtensions1.Margin = New System.Windows.Forms.Padding(4)
      Me.UclExtensions1.Name = "UclExtensions1"
      Me.UclExtensions1.Size = New System.Drawing.Size(649, 347)
      Me.UclExtensions1.TabIndex = 0
      '
      'TabPage10
      '
      Me.TabPage10.Controls.Add(Me.UclExceptions1)
      Me.TabPage10.Location = New System.Drawing.Point(4, 22)
      Me.TabPage10.Name = "TabPage10"
      Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
      Me.TabPage10.Size = New System.Drawing.Size(729, 353)
      Me.TabPage10.TabIndex = 9
      Me.TabPage10.Text = "Exceptions"
      Me.TabPage10.UseVisualStyleBackColor = True
      '
      'UclExceptions1
      '
      Me.UclExceptions1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.UclExceptions1.Location = New System.Drawing.Point(3, 3)
      Me.UclExceptions1.Name = "UclExceptions1"
      Me.UclExceptions1.Size = New System.Drawing.Size(723, 347)
      Me.UclExceptions1.TabIndex = 0
      '
      'frmMain
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.ClientSize = New System.Drawing.Size(797, 379)
      Me.Controls.Add(Me.TestTabControl1)
      Me.Controls.Add(Me.TableLayoutPanel1)
      Me.Name = "frmMain"
      Me.Text = "frmAsyncWorkerVB"
      Me.TableLayoutPanel1.ResumeLayout(False)
      CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
      CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
      Me.TestTabControl1.ResumeLayout(False)
      Me.TabPage1.ResumeLayout(False)
      Me.TabPage2.ResumeLayout(False)
      Me.TabPage3.ResumeLayout(False)
      Me.TabPage4.ResumeLayout(False)
      Me.TabPage5.ResumeLayout(False)
      Me.TabPage6.ResumeLayout(False)
      Me.TabPage7.ResumeLayout(False)
      Me.TabPage8.ResumeLayout(False)
      Me.TabPage9.ResumeLayout(False)
      Me.TabPage10.ResumeLayout(False)
      Me.ResumeLayout(False)

   End Sub
   Friend WithEvents TestTabControl1 As AsyncWorkerVB.TestTabControl
   Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
   Friend WithEvents UclPureGui1 As AsyncWorkerVB.uclPureGui
   Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
   Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
   Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
   Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
   Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
   Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
   Friend WithEvents UclStillBlocking1 As AsyncWorkerVB.uclStillBlocking
   Friend WithEvents UclNonBlocking1 As AsyncWorkerVB.uclNonBlocking
   Friend WithEvents UclBgwSimple1 As AsyncWorkerVB.uclBgwSimple
   Friend WithEvents UclBgwPrgbar1 As AsyncWorkerVB.uclBgwPrgbar
   Friend WithEvents UclAsyncWorker1 As AsyncWorkerVB.uclAsyncWorker
   Friend WithEvents UclDataBoundWorker1 As AsyncWorkerVB.uclDataBoundWorker
   Friend WithEvents UclCountDown1 As AsyncWorkerVB.uclCountDown
   Friend WithEvents UclExtensions1 As AsyncWorkerVB.uclExtensions
   Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
   Friend WithEvents UclExceptions1 As AsyncWorkerVB.uclExceptions

End Class
