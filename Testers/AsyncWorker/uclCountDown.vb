﻿Imports System.Windows.Forms

Partial Public Class uclCountDown : Inherits UserControl

   Private WithEvents _CountDown As New CountDown()

   Public Sub New()
      InitializeComponent()
      ckIsRunning.DataBindings.Add( _
         "Checked", _CountDown, "IsRunning", False, DataSourceUpdateMode.OnPropertyChanged)
   End Sub

   Private Sub _CountDown_Tick(ByVal sender As Object, ByVal e As CountDown.TickEventargs) _
         Handles _CountDown.Tick
      label1.Text = e.Counter.ToString()
      If e.Counter = 0 Then
         MessageBox.Show("Go!")
      End If
   End Sub

   Private Sub ckIsRunning_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) _
         Handles ckIsRunning.CheckedChanged
      If ckIsRunning.Checked Then _CountDown.Start(CInt(udInitValue.Value))
   End Sub

   Private Sub uclCountDown_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
      _CountDown.Dispose()
   End Sub

End Class
