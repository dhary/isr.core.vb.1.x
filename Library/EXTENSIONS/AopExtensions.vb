﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Linq.Expressions
Imports System.Reflection
Imports System.Runtime.CompilerServices

Namespace Aop

    ''' <summary>
    ''' Aspect Oriented Programming Extensions
    ''' </summary>
    ''' <license>
    ''' (c) 2012 Eugene Sadovoi.
    ''' Licensed under the Code Project Open License (CPOL).
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <remarks>
    ''' Author: Eugene Sadovoi
    ''' http://www.codeproject.com/Articles/66073/DefaultValue-Attribute-Based-Approach-to-Property?msg=3407976#xx3407976xx
    ''' </remarks>
    <Extension()> _
    Public Module Extensions

        ' Dictionary to hold type initialization methods' cache 
        Private _typesInitializers As Dictionary(Of Type, Action(Of Object)) = New Dictionary(Of Type, Action(Of Object))

        ''' <summary>
        ''' Applies the default values.
        ''' Implements precompiled setters with embedded constant values from 
        ''' <see cref="DefaultValueAttribute">default value attributes</see>.
        ''' </summary>
        ''' <param name="entity">A class with <see cref="DefaultValueAttribute">default value attributes</see>.</param>
        ''' <remarks></remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly",
            MessageId:="DefaultValueAttribute")> <System.Runtime.CompilerServices.Extension()> _
        Public Sub ApplyDefaultValues(ByVal entity As Object)

            If entity Is Nothing Then Return

            Dim setter As Action(Of Object) = Nothing

            ' Attempt to get it from cache
            If Not _typesInitializers.TryGetValue(entity.GetType(), setter) Then

                ' If no initializers are added do nothing
                setter = Sub(o)
                         End Sub

                ' Iterate through each property
                For Each prop As PropertyInfo In entity.GetType().GetProperties(BindingFlags.Public Or BindingFlags.Instance)

                    Dim dva As Expression

                    ' Skip read only properties
                    If prop.CanWrite Then

                        ' There are no more then one attribute of this type
                        Dim attr() As DefaultValueAttribute = TryCast(prop.GetCustomAttributes(GetType(DefaultValueAttribute), False), DefaultValueAttribute())

                        ' Skip properties with no DefaultValueAttribute
                        If attr IsNot Nothing AndAlso attr.Count > 0 AndAlso attr(0) IsNot Nothing Then

                            ' Build the Lambda expression
#If DEBUG Then
                            ' Make sure types do match
                            Try
                                dva = Expression.Convert(Expression.Constant(attr(0).Value), prop.PropertyType)
                            Catch e As InvalidOperationException
                                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                      "Type of DefaultValueAttribute({3}{0}{3}) does not match type of property {1}.{2}",
                                                                      attr(0).Value, entity.GetType().Name, prop.Name,
                                                                      (If(GetType(String) Is attr(0).Value.GetType(), """", "")))
                                Throw (New InvalidOperationException(message, e))
                            End Try
#Else
                            dva = Expression.Convert(Expression.Constant(attr(0).Value), prop.PropertyType)
#End If
                            Dim objectTypeParam As ParameterExpression = Expression.Parameter(GetType(Object), "this")
                            Dim setExpression As Expression = Expression.Call(Expression.TypeAs(objectTypeParam, entity.GetType()), prop.GetSetMethod(), dva)
                            Dim setLambda As Expression(Of Action(Of Object)) = Expression.Lambda(Of Action(Of Object))(setExpression, objectTypeParam)

                            ' Add this action to multicast delegate
                            setter = DirectCast(System.Delegate.Combine(setter, setLambda.Compile()), Action(Of Object))
                        End If

                    End If

                Next prop

                ' Save in the type cache
                _typesInitializers.Add(entity.GetType(), setter)

            End If

            ' Initialize member properties
            setter(entity)
        End Sub

        ''' <summary>
        ''' Resets the default values.
        ''' Implements cache of ResetValue delegates
        ''' </summary>
        ''' <param name="entity">A class with <see cref="DefaultValueAttribute">default value attributes</see>.</param>
        ''' <remarks></remarks>
        <Extension()> _
        Public Sub ResetDefaultValues(ByVal entity As Object)

            If entity Is Nothing Then Return

            Dim setter As Action(Of Object) = Nothing

            ' Attempt to get it from cache
            If Not Extensions._typesInitializers.TryGetValue(entity.GetType, setter) Then

                ' Init delegate with empty body,

                ' If no initializers are added do nothing
                setter = Function(o As Object) As Action(Of Object)
                             Return Nothing
                         End Function

                ' Go throu each property and compile Reset delegates
                For Each prop As PropertyDescriptor In TypeDescriptor.GetProperties(entity)

                    ' Add only these which values can be reset
                    If prop.CanResetValue(entity) Then
                        setter = DirectCast([Delegate].Combine(setter,
                                                               New Action(Of Object)(AddressOf prop.ResetValue)), 
                                                           Action(Of Object))
                    End If
                Next

                ' Save in the type cache
                Extensions._typesInitializers.Add(entity.GetType, setter)
            End If

            ' Initialize member properties
            setter.Invoke(entity)
        End Sub

    End Module

End Namespace

