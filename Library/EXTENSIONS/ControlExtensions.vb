﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports System.Drawing
Namespace ControlExtensions

    ''' <summary>
    ''' Includes extensions for controls.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/19/2010" by="David Hary" revision="1.2.3975.x">
    ''' Created
    ''' </history>
    Public Module [Extensions]

#Region " FROM isr.Controls "

        ''' <summary>
        ''' Gets image size for locating the image within the specified <para>Control</para>.
        ''' </summary>
        ''' <param name="control">The control.</param>
        ''' <param name="image">The image.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function GetImageSize(ByVal control As Control, ByVal image As Drawing.Image) As Size

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Size.Empty
            End If

            Dim w As Integer = Math.Min(control.Width, image.Width + 1)
            Dim h As Integer = Math.Min(control.Height, image.Height + 1)

            Return New Size(w, h)

        End Function

        ''' <summary>
        ''' Gets image location for position the image within the specified <para>Control</para>.
        ''' </summary>
        ''' <param name="control">The control.</param>
        ''' <param name="image">The image.</param>
        ''' <param name="imageAlign">The image align.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function GetImageLocation(ByVal control As Control, ByVal image As Drawing.Image, ByVal imageAlign As Drawing.ContentAlignment) As Point

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Point.Empty
            End If

            Dim size As Size = GetImageSize(control, image)

            ' get X
            Dim x As Integer = 0

            Select Case imageAlign
                Case ContentAlignment.TopLeft, ContentAlignment.MiddleLeft, ContentAlignment.BottomLeft
                    x = 0
                Case ContentAlignment.TopCenter, ContentAlignment.MiddleCenter, ContentAlignment.BottomCenter
                    x = (control.Width - size.Width) \ 2
                    If (control.Width - size.Width) Mod 2 > 0 Then x += 1
                Case Else
                    x = control.Width - size.Width
            End Select
            If x > 0 Then x -= 1

            ' get y
            Dim y As Integer = 0
            Select Case imageAlign
                Case ContentAlignment.TopLeft, ContentAlignment.TopCenter, ContentAlignment.TopRight
                    y = 0
                Case ContentAlignment.MiddleLeft, ContentAlignment.MiddleCenter, ContentAlignment.MiddleRight
                    y = (control.Height - size.Height) \ 2
                    If (control.Height - size.Height) Mod 2 > 0 Then y += 1
                Case Else
                    y = control.Height - size.Height
            End Select
            If y > 0 Then y -= 1

            Return New Point(x, y)

        End Function

        ''' <summary>
        ''' Gets image rectange within the specified <para>Control</para>.
        ''' </summary>
        ''' <param name="control">The control.</param>
        ''' <param name="image">The image.</param>
        ''' <param name="imageAlign">The image align.</param>
        ''' <param name="fillHeight">if set to <c>true</c> [fill height].</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function GetImageRect(ByVal control As Control, ByVal image As Drawing.Image,
                                     ByVal imageAlign As Drawing.ContentAlignment, ByVal fillHeight As Boolean) As Rectangle

            If image Is Nothing OrElse control Is Nothing Then
                Return Drawing.Rectangle.Empty
            End If

            Dim size As Size = GetImageSize(control, image)
            Dim location As Point = GetImageLocation(control, image, imageAlign)
            If fillHeight Then
                Return New Rectangle(location.X, location.Y, size.Width, control.Height)
            Else
                Return New Rectangle(location.X, location.Y, size.Width, size.Height)
            End If


        End Function

        ''' <summary>
        ''' Returns the control requesting help based on the mouse
        ''' position clicked.</summary>
        ''' <param name="container">Specifiesthe container control requesting the help.</param>
        ''' <returns>Returns the control upon which the operator clicked with
        '''   the help icon or nothing if no control was clicked.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ControlRequestingHelp(ByVal container As Windows.Forms.Control,
                                              ByVal helpEvent As Windows.Forms.HelpEventArgs) As Windows.Forms.Control

            If helpEvent Is Nothing OrElse container Is Nothing Then
                Return Nothing
            End If

            ' Convert screen coordinates to client coordinates
            Dim clientCoordinatePoint As System.Drawing.Point = container.PointToClient(helpEvent.MousePos)

            ' look for the control upon which the operator clicked.
            For i As Integer = 0 To container.Controls.Count
                If container.Controls.Item(i).Bounds.Contains(clientCoordinatePoint) Then
                    Return container.Controls.Item(i)
                End If
            Next

            ' if no control was located, return nothing
            Return Nothing

        End Function

        ''' <summary>Searches the combo box and selects a located item.</summary>
        ''' <param name="combo">Specifies an instance of a ComboBox.</param>
        ''' <param name="e">Specifies an instance of the 
        '''   <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
        '''   which specify the key pressed.</param>
        ''' <remarks>Use this method to search and select combo box index.</remarks>
        <Extension()> _
        Public Function SearchAndSelect(ByVal combo As Windows.Forms.ComboBox,
                                        ByVal e As System.Windows.Forms.KeyPressEventArgs) As Integer

            If combo Is Nothing Then
                Throw New ArgumentNullException("combo")
            End If
            If e IsNot Nothing AndAlso Char.IsControl(e.KeyChar) Then
                Return combo.SelectedIndex
            Else
                Dim cursorPosition As Integer = combo.SelectionStart
                Dim selectionLength As Integer = combo.SelectionLength
                Dim itemNumber As Integer = combo.FindString(combo.Text)
                If itemNumber >= 0 Then
                    ' if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber
                    combo.SelectionStart = cursorPosition
                    combo.SelectionLength = selectionLength
                    Return itemNumber
                Else
                    Return combo.SelectedIndex
                End If
            End If

        End Function

        ''' <summary>Searches the combo box and selects a located item
        '''   upon releasing a key.</summary>
        ''' <param name="combo">Specifies an instance of a ComboBox.</param>
        ''' <param name="e">Specifies an instance of the 
        '''   <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
        '''   which specify the key released.</param>
        ''' <remarks>Use this method to search and select combo box index.</remarks>
        <Extension()> _
        Public Function SearchAndSelect(ByVal combo As Windows.Forms.ComboBox,
                                        ByVal e As System.Windows.Forms.KeyEventArgs) As Integer

            If combo Is Nothing Then
                Throw New ArgumentNullException("combo")
            End If
            If e Is Nothing Then
                Throw New ArgumentNullException("e")
            End If

            If e.KeyCode.ToString.Length > 1 Then
                ' if a control character, skip
                Return combo.SelectedIndex
            Else
                Dim cursorPosition As Integer = combo.SelectionStart
                Dim selectionLength As Integer = combo.SelectionLength
                Dim itemNumber As Integer = combo.FindString(combo.Text)
                If itemNumber >= 0 Then
                    ' if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber
                    combo.SelectionStart = cursorPosition
                    combo.SelectionLength = selectionLength
                    Return itemNumber
                Else
                    Return combo.SelectedIndex
                End If
            End If

        End Function

        ''' <summary>Copies properties from a source to a destination controls.</summary>
        ''' <param name="source">
        '''   specifies an instance of the control which
        '''   properties are copied from.</param>
        ''' <param name="destination">
        '''   specifies an instance of the control which
        '''   properties are copied to.</param>
        ''' <remarks>Use this method to copy the properties of a control to a new control.</remarks>
        ''' <example>This example adds a button to a form.
        '''   <code>
        '''     Private buttons As New ArrayList()
        '''     Private Sub addButton(ByVal sourceButton As Button)
        '''       Dim newButton As New Button()
        '''       WinFormsSupport.CopyControlProperties(sourceButton, newButton)
        '''       Me.Controls.Add(newButton)
        '''       AddHandler newButton.Click, AddressOf exitButton_Click
        '''       With newButton
        '''         .Name = String.Format( System.Globalization.CultureInfo.CurrentCulture, "newButton {0}", buttons.count.toString)
        '''         .Text = .Name
        '''         .Top = .Top + .Height * (buttons.Count + 1)
        '''         buttons.Add(newButton)
        '''       End With
        '''     End Sub
        '''   </code>
        '''   To run this example, paste the code fragment into the method region
        '''   of a Visual Basic form.  Run the program by pressing F5.
        ''' </example>
        <Extension()> _
        Public Sub CopyControlProperties(ByVal source As System.Windows.Forms.Control,
                                         ByVal destination As System.Windows.Forms.Control)

            If source Is Nothing OrElse destination Is Nothing Then Return
            Dim dontCopyNames() As String = {"WindowTarget"}
            Dim pinfos(), pinfo As System.Reflection.PropertyInfo
            If Not (source Is Nothing OrElse destination Is Nothing) Then
                pinfos = source.GetType.GetProperties()
                For Each pinfo In pinfos
                    ' copy only those properties without parameters - you'll rarely need indexed properties
                    If pinfo.GetIndexParameters().Length = 0 Then
                        ' skip properties that appear in the list of disallowed names.
                        If Array.IndexOf(dontCopyNames, pinfo.Name) < 0 Then
                            ' can only copy those properties that can be read and written.  
                            If pinfo.CanRead And pinfo.CanWrite Then
                                ' set the destination based on the source.
                                pinfo.SetValue(destination, pinfo.GetValue(source, Nothing), Nothing)
                            End If
                        End If
                    End If
                Next
            End If

        End Sub

        ''' <summary>
        ''' Validates the specified control.</summary>
        ''' <param name="value">Control to be validated.</param>
        ''' <param name="container">specifies the instance of the container control which 
        ''' controls are to be validated.</param>
        ''' <returns>Returns false if failed to validate. Retursn true if control validated or not visible.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function Validate(ByVal value As Control, ByVal container As Windows.Forms.ContainerControl) As Boolean

            If value Is Nothing OrElse container Is Nothing Then Return True
            ' focus on and validate the control
            If container.Visible AndAlso value.Visible Then
                value.Focus()
                Return Not container.Validate()
            Else
                Return True
            End If

        End Function

        ''' <summary>
        ''' Validates all controls in the container control.</summary>
        ''' <param name="container">specifies the instance of the container control which 
        ''' controls are to be validated.</param>
        ''' <returns>Returns false if any control failed to validated.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValidateControls(ByVal container As Windows.Forms.ContainerControl) As Boolean

            If container Is Nothing Then Return True
            ValidateControls = True
            Dim controls As Windows.Forms.Control() = container.Controls.RetrieveControls()
            For i As Integer = 0 To controls.Length - 1
                ' validate the selected control
                If Not controls(i).Validate(container) Then
                    ValidateControls = False
                End If
            Next

        End Function

        ''' <summary>Retrieve all controls and child controls.</summary>
        ''' <param name="controls"></param>
        ''' <returns>Returns an array of controls</returns>
        ''' <remarks>Make sure to send contrll back at lowest depth first so that most
        '''   child controls are checked for things before container controls, e.g., 
        '''   a TextBox is checked before a GroupBox control.</remarks>
        ''' <history date="04/15/05" by="David Hary" revision="1.0.1566.x">
        '''   Bug fix.  Return empty controls rather than nothing if no controls.
        ''' </history>
        <Extension()> _
        Public Function RetrieveControls(ByVal controls As Windows.Forms.Control.ControlCollection) As Windows.Forms.Control()

            If controls Is Nothing Then Return Nothing

            Dim controlList As New ArrayList

            ' Dim allControls As ArrayList = New ArrayList
            Dim myQueue As Queue = New Queue
            ' add controls to the queue
            myQueue.Enqueue(controls)

            Dim myControls As Windows.Forms.Control.ControlCollection
            'Dim current As Object
            Do While myQueue.Count > 0
                ' remove and return the object at the begining of the queue
                ' current = myQueue.Dequeue()
                If TypeOf myQueue.Peek Is Windows.Forms.Control.ControlCollection Then
                    myControls = CType(myQueue.Dequeue, Windows.Forms.Control.ControlCollection)
                    If myControls.Count > 0 Then
                        For i As Integer = 0 To myControls.Count - 1
                            Dim myControl As Windows.Forms.Control = myControls.Item(i)
                            ' add this control to the array list
                            controlList.Add(myControl)
                            ' check if this control has a collection
                            If myControl.Controls IsNot Nothing Then
                                ' if so, add to the queue
                                myQueue.Enqueue(myControl.Controls)
                            End If
                        Next
                        'For Each myControl As Windows.Forms.Control In myControls
                        ' add this control to the array list
                        'controlList.Add(myControl)
                        ' check if this control has a collection
                        'If Not IsNothing(myControl.Controls) Then
                        ' if so, add to the queue
                        ' myQueue.Enqueue(myControl.Controls)
                        'End If
                        'Next
                    End If
                Else
                    myQueue.Dequeue()
                End If
            Loop
            If controlList.Count > 0 Then
                Dim allControls(controlList.Count - 1) As Windows.Forms.Control
                controlList.CopyTo(allControls)
                Return allControls
            Else
                Return New Windows.Forms.Control() {}
            End If

        End Function

#End Region

#Region " BINDING "

        ''' <summary>
        ''' Replaces the binding for the bound property of the control
        ''' </summary>
        ''' <param name="value"></param>
        ''' <param name="binding"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function Replace(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            ElseIf binding Is Nothing Then
                Throw New ArgumentNullException("binding")
            End If
            For i As Integer = 0 To value.Count - 1
                If value.Item(i).PropertyName = binding.PropertyName Then
                    value.RemoveAt(i)
                    Exit For
                End If
            Next
            value.Add(binding)
            Return binding
        End Function

        ''' <summary>
        ''' Adds a binding to the control binding collection if the biniding does not already exist on this control.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <param name="binding"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function AddIfNew(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            ElseIf binding Is Nothing Then
                Throw New ArgumentNullException("binding")
            End If
            For Each b As Binding In value
                If b.Equals(binding) Then
                    Return binding
                End If
            Next
            value.Add(binding)
            Return binding
        End Function

        ''' <summary>
        ''' Adds a binding to the control. Disables the control while adding the binding to allow the
        ''' disabling of control events while the binding is added. Note that the control event of value 
        ''' change occurs before the bound count increments so the bound count cannot be used to determine
        ''' the control bindable status.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <param name="binding"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentAdd(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            ElseIf binding Is Nothing Then
                Throw New ArgumentNullException("binding")
            End If
            Dim isEnabled As Boolean = value.Control.Enabled
            value.Control.Enabled = False
            value.Add(binding)
            value.Control.Enabled = isEnabled
            Return binding
        End Function

#End Region

#Region " COMBO BOX SETTERS "

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSelectItem), New Object() {control, value})
            Else
                control.SelectedItem = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="key">The selected item key.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return SafeSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSilentSelectItem), New Object() {control, value})
            Else
                SilentSelectItem(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="key">The selected item key.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return SafeSilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Dim wasEnabled As Boolean = control.Enabled
            control.Enabled = False
            control.SelectedItem = value
            control.Enabled = wasEnabled
            Return value
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
        ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">Combo box control.</param>
        ''' <param name="key">The selected item key.</param>
        ''' <param name="value">The selected item value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return SilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function


        ''' <summary>
        ''' Sets the <see cref="ComboBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As ComboBox, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If

            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, String)(AddressOf SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ComboBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeSilentTextSetter(ByVal control As ComboBox, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, String)(AddressOf SafeSilentTextSetter), New Object() {control, value})
            Else
                SilentTextSetter(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ComboBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SilentTextSetter(ByVal control As ComboBox, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            Dim enabled As Boolean = control.Enabled
            control.Enabled = False
            control.Text = value
            control.Enabled = enabled
            Return value
        End Function


#End Region

#Region " CHECK BOX SETTERS "

        ''' <summary>
        ''' Sets the <see cref="Control">check box</see> checked value to the 
        ''' <paramref name="value">value</paramref>.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">Check box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Dim wasEnabled As Boolean = control.Enabled
            control.Enabled = False
            control.Checked = value
            control.Enabled = wasEnabled
            Return value
        End Function


        ''' <summary>
        ''' Sets the <see cref="Control">check box</see> checked value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">Check box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf SafeSilentCheckedSetter), New Object() {control, value})
            Else
                SilentCheckedSetter(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="Control">check box</see> checked value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Check box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf SafeCheckedSetter), New Object() {control, value})
            Else
                control.Checked = value
            End If
            Return value
        End Function

#End Region

#Region " CONTROL SAFE SETTERS "

        ''' <summary>
        ''' Safely run an action from a thread.
        ''' </summary>
        ''' <typeparam name="T">Specifies a control type.</typeparam>
        ''' <param name="control">Specifies reference to a control.</param>
        ''' <param name="action">Spoecifies reference to a function implementing the action</param>
        ''' <remarks>
        ''' <example>
        ''' <code>
        ''' </code>
        ''' </example>
        ''' </remarks>
        <Extension()> _
        Public Sub SafeThreadAction(Of T As Control)(ByVal control As T, ByVal action As Action(Of T))
            If control IsNot Nothing AndAlso action IsNot Nothing Then
                If control.InvokeRequired Then
                    control.BeginInvoke(action, control)
                Else
                    action(control)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Safely run a setter action from a thread.
        ''' </summary>
        ''' <typeparam name="T">Specifies a control type.</typeparam>
        ''' <typeparam name="TType">Specifies the set type.</typeparam>
        ''' <param name="control">Specifies reference to a control.</param>
        ''' <param name="action">Spoecifies reference to a function implementing the action</param>
        ''' <param name="value">Specifies a value to set.</param>
        ''' <remarks>
        ''' <example>
        ''' <code>
        ''' </code>
        ''' </example>
        ''' </remarks>
        <Extension()> _
        Public Sub SafeThreadSetter(Of T As Control, TType)(ByVal control As T, ByVal action As Action(Of T, TType), ByVal value As TType)
            If control IsNot Nothing AndAlso action IsNot Nothing Then
                If control.InvokeRequired Then
                    control.BeginInvoke(action, control, value)
                Else
                    action(control, value)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Safely gets a value from a thread.
        ''' </summary>
        ''' <typeparam name="T">Specifies a control type.</typeparam>
        ''' <typeparam name="TType">A type which to return</typeparam>
        ''' <param name="control">Specifes reference to a control.</param>
        ''' <param name="getter">Reference to a getter function</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' <example>
        ''' <code>
        ''' </code>
        ''' </example>
        ''' </remarks>
        <Extension()> _
        Public Function SafeThreadGetter(Of T As Control, TType)(ByVal control As T, ByVal getter As Func(Of T, TType)) As TType
            If control IsNot Nothing AndAlso getter IsNot Nothing Then
                If control.InvokeRequired Then
                    Dim result As IAsyncResult = control.BeginInvoke(getter, control)
                    Dim result2 As Object = control.EndInvoke(result)
                    Return CType(result2, TType)
                Else
                    Return getter(control)
                End If
            End If
        End Function

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see> center middle using the control font.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.Control, ByVal value As String)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <param name="textAlign">Where the text is to be placed</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            SafelyDrawText(control, value, textAlign, control.Font)
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <param name="textAlign">Where the text is to be placed</param>
        ''' <param name="textFont">The font the text should be drawn in</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment, _
                              ByVal textFont As System.Drawing.Font)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, String, Drawing.ContentAlignment, Drawing.Font)(AddressOf SafelyDrawText), New Object() {control, value, textAlign, textFont})
            Else
                Using gr As Drawing.Graphics = control.CreateGraphics()
                    Const margin As Single = 0
                    Dim x As Single = margin
                    Dim y As Single = (control.Height - gr.MeasureString(value, textFont).Height) / 2.0F
                    If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.MiddleCenter OrElse textAlign = Drawing.ContentAlignment.TopCenter Then
                        x = (control.Width - gr.MeasureString(value, textFont).Width) / 2.0F
                    ElseIf textAlign = Drawing.ContentAlignment.BottomRight OrElse textAlign = Drawing.ContentAlignment.MiddleRight OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                        x = control.Width - gr.MeasureString(value, textFont).Width - margin
                    End If
                    If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.BottomLeft OrElse textAlign = Drawing.ContentAlignment.BottomRight Then
                        y = control.Height - gr.MeasureString(value, textFont).Height - margin
                    ElseIf textAlign = Drawing.ContentAlignment.TopCenter OrElse textAlign = Drawing.ContentAlignment.TopLeft OrElse textAlign = Drawing.ContentAlignment.TopRight Then
                        y = margin
                    End If
                    y = Math.Max(y, margin)
                    x = Math.Max(x, margin)
                    Using sb As New Drawing.SolidBrush(control.ForeColor)
                        gr.DrawString(value, textFont, sb, New Drawing.PointF(x, y))
                    End Using
                End Using
            End If

        End Sub

        ''' <summary>
        ''' Sets the <see cref="Control">control</see> enabled value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeEnabledSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If

            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, Boolean)(AddressOf SafeEnabledSetter), New Object() {control, value})
            Else
                control.Enabled = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="Control">control</see> enabled value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeVisibleSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, Boolean)(AddressOf SafeVisibleSetter), New Object() {control, value})
            Else
                control.Visible = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As Control, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, String)(AddressOf SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeSilentTextSetter(ByVal control As Control, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, String)(AddressOf SafeSilentTextSetter), New Object() {control, value})
            Else
                SilentTextSetter(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeSilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeSilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SilentTextSetter(ByVal control As Control, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            Dim enabled As Boolean = control.Enabled
            control.Enabled = False
            control.Text = value
            control.Enabled = enabled
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " DATA GRID VIEW "

        ''' <summary>
        ''' Hides the columns by the <paramref name="columnNames">column names</paramref>.
        ''' </summary>
        ''' <param name="grid">The grid.</param>
        ''' <param name="columnNames">The ciolumn names.</param>
        ''' <remarks></remarks>
        <Extension()> _
        Public Sub HideColumns(ByVal grid As DataGridView, ByVal columnNames As String())
            If grid IsNot Nothing AndAlso columnNames IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If Not columnNames.Contains(column.Name) Then
                        column.Visible = False
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Replaces the column header text with one that has spaces between lower and upper case characters.
        ''' </summary>
        ''' <param name="column">The column</param>
        ''' <remarks></remarks>
        <Extension()> _
        Public Sub ParseHeaderText(ByVal column As DataGridViewColumn)
            If column IsNot Nothing Then
                column.HeaderText = isr.Core.StringExtensions.SplitWords(column.HeaderText)
            End If
        End Sub

        ''' <summary>
        ''' Replaces the column headers text with one that has spaces between lower and upper case charactes.
        ''' </summary>
        ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
        ''' <remarks></remarks>
        <Extension()> _
        Public Sub ParseHeaderText(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    column.ParseHeaderText()
                Next
            End If
        End Sub

        ''' <summary>
        ''' Replaces the column headers text with one that has spaces between lower and upper case charactes.
        ''' Operates only on visible headers.
        ''' </summary>
        ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
        ''' <remarks></remarks>
        <Extension()> _
        Public Sub ParseVisibleHeaderText(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If column.Visible Then
                        column.ParseHeaderText()
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Replaces an existing column with a combo box column and returs reference to this column.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ReplaceWithComboColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewComboBoxColumn
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New System.Windows.Forms.DataGridViewComboBoxColumn
            Try
                gridColumn.Name = column.Name
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                'gridColumn.IsDataBound = column.IsDataBound
                grid.Columns.Remove(column)
                ' this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                If gridColumn IsNot Nothing Then
                    gridColumn.Dispose()
                    gridColumn = Nothing
                End If
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Replaces an existing column with a check box column and returs reference to this column.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ReplaceWithCheckColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewCheckBoxColumn
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            Dim gridColumn As New System.Windows.Forms.DataGridViewCheckBoxColumn
            Try
                gridColumn.Name = column.Name
                gridColumn.DataPropertyName = column.DataPropertyName
                gridColumn.HeaderText = column.HeaderText
                gridColumn.ThreeState = True
                grid.Columns.Remove(column)
                grid.Columns.Insert(column.Index, gridColumn)
                Return gridColumn
            Catch
                If gridColumn IsNot Nothing Then
                    gridColumn.Dispose()
                    gridColumn = Nothing
                End If
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Moves an existing column to the specified location.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function MoveTo(ByVal grid As DataGridView, ByVal columnName As String, ByVal location As Integer) As DataGridViewColumn
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
            grid.Columns.Remove(column)
            grid.Columns.Insert(location, column)
            Return column
        End Function

        ''' <summary>
        ''' Returns the row selected based on the column value.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function FindRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As DataGridViewRow
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim foundRow As DataGridViewRow = Nothing
            For Each r As DataGridViewRow In grid.Rows
                If columnValue.Equals(r.Cells(columnName).Value) Then
                    foundRow = r
                    Exit For
                End If
            Next
            Return foundRow
        End Function

        ''' <summary>
        ''' Selects the row.
        ''' </summary>
        ''' <typeparam name="T">The type of the column value</typeparam>
        ''' <param name="grid">The grid.</param>
        ''' <param name="columnName">Name of the column.</param>
        ''' <param name="columnValue">The column value.</param>
        ''' <returns>True if the row was selected.</returns>
        <Extension()> _
        Public Function SelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim foundRow As DataGridViewRow = FindRow(Of T)(grid, columnName, columnValue)
            If foundRow Is Nothing Then
                Return False
            Else
                foundRow.Selected = True
                Return True
            End If
        End Function

        ''' <summary>
        ''' Silently selects a grid row.
        ''' The control is disabled when set so that the 
        ''' handling of the changed event can be skipped.
        ''' </summary>
        ''' <typeparam name="T">The type of the column value</typeparam>
        ''' <param name="grid">The grid.</param>
        ''' <param name="columnName">Name of the column.</param>
        ''' <param name="columnValue">The column value.</param>
        ''' <returns>True if the row was selected.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentSelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
            If grid Is Nothing Then
                Throw New ArgumentNullException("grid")
            ElseIf String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim enabled As Boolean = grid.Enabled
            Try
                grid.Enabled = False
                Return SelectRow(Of T)(grid, columnName, columnValue)
            Catch
                Throw
            Finally
                grid.Enabled = enabled
            End Try
        End Function

#End Region

#Region " LABEL SETTERS "

        ''' <summary>
        ''' Sets the <see cref="Label">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As Label, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Label, String)(AddressOf SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="Label">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As Label, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " LIST CONTROLS: COMBO BOX; LIST BOX "

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> _
        Public Function SafeSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, T)(AddressOf SafeSelectValue), New Object() {control, value})
            Else
                control.SelectedValue = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> _
        Public Function SilentSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Dim enabled As Boolean = control.Enabled
            control.Enabled = False
            control.SelectedValue = value
            control.Enabled = enabled
            Return value
        End Function

        ''' <summary>
        ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="control">The control.</param>
        ''' <param name="value">The value.</param><returns></returns>
        <Extension()> _
        Public Function SafeSilentTextSetter(Of T)(ByVal control As ListControl, ByVal value As T) As T
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSilentTextSetter), New Object() {control, value})
            Else
                SafeSilentTextSetter(control, value)
            End If
            Return value
        End Function

#End Region

#Region " NUMERIC UP DOWN SETTERS "

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> scaled by the 
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Retuns limited value (control value divided by the scaler).
        ''' The setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The control value divided by the scaler.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Return SafeValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> scaled by the 
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Retuns limited value (control value divided by the scaler).
        ''' This setter is thread safe.
        ''' Retuns limited value (control value divided by the scaler).
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The control value divided by the scaler.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return SafeSilentValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf SafeValueSetter), New Object() {control, value})
            Else
                ValueSetter(control, value)
            End If
            Return control.Value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' Retuns limited value (control value divided by the scaler).
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf SafeValueSetter), New Object() {control, value})
            Else
                SilentValueSetter(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
            Return SafeValueSetter(control, CDec(value))
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' Retuns limited value (control value divided by the scaler).
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
            Return SafeSilentValueSetter(control, CDec(value))
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> scaled by the 
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Retuns limited value (control value divided by the scaler).
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The control value divided by the scaler.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return ValueSetter(control, scalar * value) / scalar
#If False Then
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, scalar * value))
      Return control.Value / scalar
#End If
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> scaled by the 
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Retuns limited value (control value divided by the scaler).
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The control value divided by the scaler.</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return SilentValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
            Return control.Value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Dim wasEnabled As Boolean = control.Enabled
            control.Enabled = False
            ValueSetter(control, value)
            control.Enabled = wasEnabled
            Return control.Value
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
            Return ValueSetter(control, CDec(value))
        End Function

        ''' <summary>
        ''' Sets the <see cref="NumericUpDown">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Decimal
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Dim wasEnabled As Boolean = control.Enabled
            control.Enabled = False
            ValueSetter(control, CDec(value))
            control.Enabled = wasEnabled
            Return control.Value
        End Function


#End Region

#Region " TOOL STRIP PROGRESS BAR SETTERS "

        ''' <summary>
        ''' Sets the <see cref="ToolStripProgressBar">control</see> Enabled to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeEnabledSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf SafeEnabledSetter), New Object() {control, value})
            Else
                control.Enabled = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ToolStripProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of ToolStripProgressBar, Integer)(AddressOf SafeValueSetter), New Object() {control, value})
            Else
                value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
                control.Value = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Updates the <see cref="ToolStripProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueUpdater(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Value <> value Then
                Return SafeValueSetter(control, value)
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ToolStripItem">control</see> visible to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeVisibleSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf SafeVisibleSetter), New Object() {control, value})
            Else
                control.Visible = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
            Return CInt(control.Value)
        End Function

#End Region

#Region " PROGRESS BAR SETTERS "

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see> center middle using the control font.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <param name="textAlign">Where the text is to be placed</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
                value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
            End If
            SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, control.Font)
        End Sub

        ''' <summary>
        ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
        ''' </summary>
        ''' <param name="control">The target progress bar to add text into</param>
        ''' <param name="value">The text to add into the progress bar. 
        ''' Leave null or empty to automatically add the percent.</param>
        ''' <param name="textAlign">Where the text is to be placed</param>
        ''' <param name="textFont">The font the text should be drawn in</param>
        ''' <remarks>
        ''' Call this method after changing the progress bar's value. 
        ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
        ''' call the Refresh method of the progress bar before calling this method.
        ''' </remarks>
        ''' <overloads>
        ''' Draws text into a control.
        ''' </overloads>
        ''' <shoutouts>
        ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
        ''' </shoutouts>
        <Extension()> _
        Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment, _
                                  ByVal textFont As System.Drawing.Font)
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                control.Refresh()
                Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
                value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
            End If
            SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, textFont)
        End Sub

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
            Return CInt(control.Value)
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function ValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Value <> value Then
                Return ValueSetter(control, value)
            End If
            Return CInt(control.Value)
        End Function

        ''' <summary>
        ''' Sets the <see cref="ProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of ProgressBar, Integer)(AddressOf SafeValueSetter), New Object() {control, value})
            Else
                value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
                control.Value = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Updates the <see cref="ProgressBar">control</see> value to the 
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The extended numeric up down control.</param>
        ''' <param name="value">The candidate value.</param>
        ''' <returns>The limited value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.Value <> value Then
                Return SafeValueSetter(control, value)
            End If
            Return value
        End Function

#End Region

#Region " STATUS BAR SETTERS "

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If control.Parent.InvokeRequired Then
                control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="TextBox">control</see> text to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function SafeToolTipTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If control.Parent.InvokeRequired Then
                control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf SafeToolTipTextSetter), New Object() {control, value})
            Else
                control.ToolTipText = value
            End If
            Return value
        End Function

#End Region

#Region " TEXT BOX SETTERS "

        ''' <summary>
        ''' Appends <paramref name="value">text</paramref> to the 
        ''' <see cref="TextBox">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function Append(ByVal control As TextBox, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            Else
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBox, String)(AddressOf Append), New Object() {control, value})
                Else
                    control.SelectionStart = control.Text.Length
                    control.SelectionLength = 0
                    If control.SelectionStart = 0 Then
                        control.SelectedText = value
                    Else
                        control.SelectedText = Environment.NewLine & value
                    End If
                    control.SelectionStart = control.Text.Length
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Appends <paramref name="value">text</paramref> to the 
        ''' <see cref="TextBox">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function Append(ByVal control As TextBox, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Append(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary>
        ''' Prepends <paramref name="value">text</paramref> to the 
        ''' <see cref="TextBox">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function Prepend(ByVal control As TextBox, ByVal value As String) As String
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If String.IsNullOrEmpty(value) Then
                value = ""
            Else
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBox, String)(AddressOf Append), New Object() {control, value})
                Else
                    control.SelectionStart = 0
                    control.SelectionLength = 0
                    control.SelectedText = value & Environment.NewLine
                    control.SelectionLength = 0
                End If
            End If
            Return value
        End Function

        ''' <summary>
        ''' Prepends <paramref name="value">text</paramref> to the 
        ''' <see cref="TextBox">control</see>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">The text box control.</param>
        ''' <param name="format">The text format.</param>
        ''' <param name="args">The format arguments.</param>
        ''' <returns>value</returns>
        ''' <remarks>The value is set to empty if null or empty.</remarks>
        <Extension()> _
        Public Function Prepend(ByVal control As TextBox, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Prepend(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " TOOL STRIP BUTTON SETTERS "

        ''' <summary>
        ''' Sets the <see cref="ToolStripButton">Tool Strip Button</see> checked value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' The setter disables the control before altering the checked state allowing the 
        ''' control code to use the enabled state for preventing the execution of the control
        ''' checked change actions.
        ''' </summary>
        ''' <param name="control">Check box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeSilentCheckedSetter(ByVal control As ToolStripButton, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.GetCurrentParent.InvokeRequired Then
                control.GetCurrentParent.Invoke(New Action(Of ToolStripButton, Boolean)(AddressOf SafeSilentCheckedSetter), New Object() {control, value})
            Else
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.Checked = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary>
        ''' Sets the <see cref="ToolStripButton">Tool Strip Button</see> checked value to the 
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe.
        ''' </summary>
        ''' <param name="control">Check box control.</param>
        ''' <param name="value">The value.</param>
        ''' <returns>value</returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function SafeCheckedSetter(ByVal control As ToolStripButton, ByVal value As Boolean) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            If control.GetCurrentParent.InvokeRequired Then
                control.GetCurrentParent.Invoke(New Action(Of ToolStripButton, Boolean)(AddressOf SafeCheckedSetter), New Object() {control, value})
            Else
                control.Checked = value
            End If
            Return value
        End Function

#End Region

    End Module

End Namespace
