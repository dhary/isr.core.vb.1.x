﻿Imports System.Runtime.CompilerServices
Namespace ArrayExtensions
    ''' <summary>
    ''' Includes extensions for native arrays.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
    ''' Created
    ''' </history>
    Public Module [Extensions]

        ''' <summary>
        ''' Appends a list of elements to the end of an array
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="values">The array</param>
        ''' <param name="items"></param>
        ''' <returns>Modified array.</returns>
        <Extension()> _
        Public Function Append(Of T)(ByVal values As T(), ByVal ParamArray items As T()) As T()

            If values Is Nothing Then Return items
            If items Is Nothing Then Return values

            Dim oldLength As Integer = values.Length

            ' make room for new items
            System.Array.Resize(values, oldLength + items.Length)

            For i As Integer = 0 To items.Length - 1
                values(oldLength + i) = items(i)
            Next i
            Return values

        End Function

        ''' <summary>
        ''' Determines if the two specified arrays have the same values.
        ''' </summary>
        ''' <param name="left">The left value.</param>
        ''' <param name="right">The right value.</param><returns></returns>
        ''' <remarks>
        ''' <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used because it expects the two 
        ''' entities to be the same for equality.
        ''' </remarks>
        <Extension()> _
        Public Function Equals(Of T)(ByVal left As T(), ByVal right As T()) As Boolean
            Dim result As Boolean = True
            If left Is Nothing Then
                result = right Is Nothing
            ElseIf right Is Nothing Then
                result = False
            Else
                For i As Integer = 0 To left.Length - 1
                    If Not left(i).Equals(right(i)) Then
                        result = False
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

        ''' <summary>
        ''' Remove an Array at a specific Location.
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="values">The Array Object</param>
        ''' <param name="index">index to remove at</param>
        ''' <returns>Modified array.</returns>
        <Extension()> _
        Public Function RemoveAt(Of T)(ByVal values As T(), ByVal index As Integer) As T()

            If index < 0 OrElse values Is Nothing OrElse values.Length = 0 OrElse values.Length <= (index + 1) Then
                Return values
            End If

            ' move everything from the index on to the left one then remove last empty
            For i As Integer = index + 1 To values.Length - 1
                values(i - 1) = values(i)
            Next i

            System.Array.Resize(values, values.Length - 1)

            Return values

        End Function

        ''' <summary>
        ''' Remove all elements in an array satisifying a predicate
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="values">The Array Object</param>
        ''' <param name="condition">A Predicate when the element shall get removed under.</param>
        ''' <returns>Modified array.</returns>
        <Extension()> _
        Public Function RemoveAll(Of T)(ByVal values As T(), ByVal condition As Predicate(Of T)) As T()

            If condition Is Nothing OrElse values Is Nothing OrElse values.Length = 0 Then
                Return values
            End If

            Dim Count As Integer = 0
            For i As Integer = 0 To values.Length - 1
                If condition(values(i)) Then
                    RemoveAt(Of T)(values, i)
                    Count += 1
                End If
            Next i
            Return values

        End Function

    End Module

End Namespace
