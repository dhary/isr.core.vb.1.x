﻿''' <summary>
''' Includes generic fucntion.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
Public Module GenericFunctions

  ''' <summary>
  ''' Makes the standard immediate if function type safe.
  ''' </summary>
  ''' <typeparam name="T">Specifies the returned value.</typeparam>
  ''' <param name="condition">Specifies the condition.</param>
  ''' <param name="truePart">Specifies the value to return if the condition is true.</param>
  ''' <param name="falsePart">Specifies the value to return if the condition is false.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IIf(Of T)(ByVal condition As Boolean, ByVal truePart As T, ByVal falsePart As T) As T
    If condition Then
      Return truePart
    Else
      Return falsePart
    End If
  End Function

End Module
