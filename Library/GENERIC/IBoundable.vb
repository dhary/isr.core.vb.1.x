﻿''' <summary>
''' The contract implemented by entities with bounds.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="05/15/2009" by="David Hary" revision="1.1.3422.x">
''' Created
''' </history>
Public Interface IBoundable(Of T As {IComparable(Of T), IEquatable(Of T)})

  ''' <summary>Gets or sets the high limit.
  ''' </summary>
  Property HighLimit() As T

  ''' <summary>
  ''' Gets the condition telling if the value is lower than the <see cref="HighLimit">high limit</see>.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property IsHigh() As Boolean

  ''' <summary>
  ''' Gets the condition telling if the value is lower than the <see cref="LowLimit">low limit</see>.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property IsLow() As Boolean

  ''' <summary>Gets or sets the low limit.
  ''' </summary>
  Property LowLimit() As T

End Interface
