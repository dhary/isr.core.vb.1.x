﻿''' <summary>
''' The contract implemented by the Observers with Synchronization.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public Interface ISyncObserver

  Inherits System.ComponentModel.ISynchronizeInvoke, IObserver

  ''' <summary>
  ''' Gets reference to the sync publisher.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property IPublisher() As isr.Core.ISyncPublisher

End Interface
