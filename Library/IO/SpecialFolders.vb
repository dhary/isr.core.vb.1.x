﻿''' <summary>
''' Helper files for special folders.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="10/22/09" by="David Hary" revision="1.2.3582.x">
''' Created
''' </history>
Public NotInheritable Class SpecialFolders

  ''' <summary>Prevents initialization.</summary>
  Private Sub New()
    MyBase.New()
  End Sub

  ''' <summary>
  ''' Returns the application data folder using the application company and product name or directory path.
  ''' In design mode the data folder is the program folder such as c:\Program File\ISR\ISR Provers 7.0\Program
  ''' In production, this is the application data folder product name
  ''' such as Application Data\ISR\ISR Provers 7.0\x.yy.xxxx under XP or 
  ''' ProgramData\ISR\ISR Provers 7.0\x.yy.xxxx under Vista
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean, _
                                                        ByVal allUsers As Boolean) As String

    Dim pathName As String = ""

    If useApplicationFolder Then

      pathName = My.Application.Info.DirectoryPath

    Else

      Dim specialPath As String = ""
      If allUsers Then
        specialPath = My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData
      Else
        specialPath = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData
      End If

      ' get the application folder.
      specialPath = My.Computer.FileSystem.GetParentPath(specialPath)
      ' get the top folder so that we can create our own path invariant of the executing assembly.
      specialPath = My.Computer.FileSystem.GetParentPath(specialPath)

      ' use the company name and product name to the the actual path.
      pathName = System.IO.Path.Combine(specialPath, My.Application.Info.CompanyName)
      pathName = System.IO.Path.Combine(pathName, My.Application.Info.ProductName)
      pathName = System.IO.Path.Combine(pathName, My.Application.Info.Version.ToString(3))

    End If

    ' return the full paht for the folder.
    Return pathName

  End Function

  ''' <summary>
  ''' Returns a standard data folder name.  
  ''' In design mode the data folder is a sibling to the program folder.
  ''' In production, the data folder resides under the application data folder product name.
  ''' </summary>
  ''' <param name="folderTitle"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean, _
                                                        ByVal allUsers As Boolean, _
                                                        ByVal folderTitle As String, _
                                                        ByVal useSiblingFolder As Boolean) As String

    Dim dataPath As String = SpecialFolders.BuildApplicationDataFolderName(useApplicationFolder, allUsers)
    If useSiblingFolder Then
      dataPath = My.Computer.FileSystem.GetParentPath(dataPath)
    End If
    Return System.IO.Path.Combine(dataPath, folderTitle)

  End Function

End Class
