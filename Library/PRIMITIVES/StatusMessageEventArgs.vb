''' <summary>
''' Defines an event arguments class for status messages.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/17/2011" by="David Hary" revision="1.0.4368.x">
''' Created based on legacy core message event arguments.
''' </history>
Public Class StatusMessageEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor.</summary>
    Public Sub New()
        Me.New("")
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="statusMessage">Specifies the Status Message.
    ''' </param>
    ''' <remarks></remarks>
    Public Sub New(ByVal statusMessage As String)
        MyBase.new()
        _StatusMessage = statusMessage
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="format">Specifies the message formatting string</param>
    ''' <param name="args">Specifies the arguments.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    Private _StatusMessage As String
    ''' <summary>
    ''' Gets the Status Message.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property StatusMessage() As String
        Get
            Return _StatusMessage
        End Get
    End Property

    ''' <summary>
    ''' Gets an empty <see cref="StatusMessageEventArgs">event arguments</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Shadows ReadOnly Property Empty() As StatusMessageEventArgs
        Get
            Return New StatusMessageEventArgs
        End Get
    End Property

#End Region

End Class