﻿''' <summary>
''' Defines an event arguments class for reporting progress.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/17/2011" by="David Hary" revision="1.0.4368.x">
''' Created.
''' </history>
Public Class ProgressChangedEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor.</summary>
    Public Sub New()
        Me.New(0)
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="percentProgress">Specifies the percent progress to report.
    ''' </param>
    ''' <remarks></remarks>
    Public Sub New(ByVal percentProgress As Integer)
        MyBase.new()
        _PercentProgress = percentProgress
    End Sub


#End Region

#Region " PROPERTIES "

    Private _PercentProgress As Integer
    ''' <summary>
    ''' Gets the percent progress.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property PercentProgress() As Integer
        Get
            Return _PercentProgress
        End Get
    End Property

    ''' <summary>
    ''' Gets an empty <see cref="ProgressChangedEventArgs">event arguments</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Shadows ReadOnly Property Empty() As ProgressChangedEventArgs
        Get
            Return New ProgressChangedEventArgs
        End Get
    End Property

#End Region

End Class

