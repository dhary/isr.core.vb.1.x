''' <summary>
''' Defines an event arguments class for <see cref="TraceMessage">trace message</see>.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/17/2011" by="David Hary" revision="1.0.4368.x">
''' Created based on legacy core message event arguments.
''' </history>
Public Class TraceMessageEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Default constructor.</summary>
    Public Sub New()
        Me.New(TraceEventType.Information, "", "")
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="details">Specifies the Trace Message details.
    ''' </param>
    ''' <remarks></remarks>
    Public Sub New(ByVal details As TraceMessage)
        MyBase.new()
        _TraceMessage = details
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="format">Specifies the message formatting string</param>
    ''' <param name="args">Specifies the arguments.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(TraceEventType.Information, format, args)
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace level</see></param>
    ''' <param name="format">Specifies the message formatting string</param>
    ''' <param name="args">Specifies the arguments.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal trace As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(New TraceMessage(trace, format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    Private _TraceMessage As TraceMessage
    ''' <summary>
    ''' Gets the Trace Message.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property TraceMessage() As TraceMessage
        Get
            Return _TraceMessage
        End Get
    End Property

    ''' <summary>
    ''' Gets an empty <see cref="TraceMessageEventArgs">event arguments</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Shadows ReadOnly Property Empty() As TraceMessageEventArgs
        Get
            Return New TraceMessageEventArgs
        End Get
    End Property

#End Region

End Class