﻿''' <summary>
''' The contract implemented by presettable and cacheable String Values.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface IPresettableCacheableString

  Inherits IUniquePresettableUpdatable, ICacheableString, IPresettable, IResettable, IClearable

  Property ClearValue() As String

  Property DefaultValue() As String

  Property PresetValue() As String

End Interface


''' <summary>
''' Implements a <see cref="IPresettableCacheableString">presettable Cacheable string value</see>.
''' </summary>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Class PresettableCacheableString

  Inherits CacheableString
  Implements IPresettableCacheableString

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableString" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New(name)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableString" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As IPresettableCacheableString)
    MyBase.New(model)
    Me._clearValue = model.ClearValue
    Me._defaultValue = model.DefaultValue
    Me._presetValue = model.PresetValue
  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="ICacheableString">cacheable string</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Public Overloads Shared Function Equals(ByVal left As ICacheableString, ByVal right As ICacheableString) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
  End Function

#End Region

#Region " EQUALS "

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return MyBase.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As String, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="ClearValue">clear value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Clear() As Boolean Implements IClearable.Clear
    If Not String.IsNullOrEmpty(Me.ClearValue) Then
      MyBase.ActualValue = Me.ClearValue
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="PresetValue">preset value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    If Not String.IsNullOrEmpty(Me.PresetValue) Then
      MyBase.ActualValue = Me.PresetValue
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="DefaultValue">default value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    If Not String.IsNullOrEmpty(Me.DefaultValue) Then
      MyBase.ActualValue = Me.DefaultValue
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IPRESETTABLE CACHE "

  Private _clearValue As String
  ''' <summary>
  ''' Gets or sets the clear value.
  ''' </summary>
  ''' <value>
  ''' The clear value.
  ''' </value>
  Public Property ClearValue() As String Implements IPresettableCacheableString.ClearValue
    Get
      Return _clearValue
    End Get
    Set(ByVal value As String)
      If String.IsNullOrEmpty(Me.ClearValue) OrElse Not Me.ClearValue.Equals(value) Then
        _clearValue = value
        Me.OnPropertyChanged("ClearValue")
      End If
    End Set
  End Property

  Private _defaultValue As String
  ''' <summary>
  ''' Gets or sets the default value.
  ''' </summary>
  ''' <value>
  ''' The default value.
  ''' </value>
  Public Property DefaultValue() As String Implements IPresettableCacheableString.DefaultValue
    Get
      Return _defaultValue
    End Get
    Set(ByVal value As String)
      If String.IsNullOrEmpty(Me.DefaultValue) OrElse Not Me.DefaultValue.Equals(value) Then
        _defaultValue = value
        Me.OnPropertyChanged("DefaultValue")
      End If
    End Set
  End Property

  Private _presetValue As String
  ''' <summary>
  ''' Gets or sets the preset value.
  ''' </summary>
  ''' <value>
  ''' The preset value.
  ''' </value>
  Public Property PresetValue() As String Implements IPresettableCacheableString.PresetValue
    Get
      Return _presetValue
    End Get
    Set(ByVal value As String)
      If String.IsNullOrEmpty(Me.PresetValue) OrElse Not Me.PresetValue.Equals(value) Then
        _presetValue = value
        Me.OnPropertyChanged("PresetValue")
      End If
    End Set
  End Property

#End Region

End Class

