﻿''' <summary>
''' Defines the interface for connecting and disconnecting from an instrument.
''' This interface is extended by the <see cref="isr.Core.IConnectResource">IConnectResource</see>
''' in allowing the use of resource names for working with VISA and adding events.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="09/02/2009" by="David Hary" revision="1.2.3532.x">
''' Adds a connect method with string address to allow working with IP addresses.
''' </history>
Public Interface IConnect

  ''' <summary>
  ''' Connects this instance using an existing resource name.
  ''' </summary><returns></returns>
  Function Connect() As Boolean

  ''' <summary>
  ''' Connects and returns true if connected.  Use for connecting to interfaces
  ''' requiring a resource name.
  ''' </summary>
  ''' <param name="resourceName">Specifies the instrument reasource name</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' <history date="09/02/2009" by="David Hary" revision="3.0.3532.x">
  ''' Allows connecting using a string-based address.
  ''' </history>
  Function Connect(ByVal resourceName As String) As Boolean

  ''' <summary>
  ''' Connects and returns true if connected.  UYse for connecting to interfaces
  ''' requiring an address
  ''' </summary>
  ''' <param name="address">Specifies the instrument address</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Connect(ByVal address As Integer) As Boolean

  ''' <summary>
  ''' Gets or sets the time out allowed for connecting.
  ''' </summary>
  Property Timeout() As Integer

  ''' <summary>Gets or sets the connection status.</summary>
  ''' <value><c>Connected</c> is a <see cref="Boolean"/> property that is True if the 
  ''' connected (open).</value>
  ''' <history date="02/07/06" by="David Hary" revision="1.00.2228.x">
  '''   Rename
  ''' </history>
  ReadOnly Property IsConnected() As Boolean

  ''' <summary>Disconnects and returns true if disconnected.</summary>
  ''' <returns><c>True</c> if the instance disconnected.</returns>
  Function Disconnect() As Boolean

  ''' <summary>
  ''' Occurs when the connection changed.
  ''' </summary>
  Event ConnectionChanged As EventHandler(Of EventArgs)

  ''' <summary>
  ''' Raises the <see cref="E:ConnectionChanged" /> event.
  ''' </summary>
  ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
  Sub OnConnectionChanged(ByVal e As System.EventArgs)

End Interface
