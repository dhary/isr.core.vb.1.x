﻿''' <summary>
''' Interfaces user log on/off functionality.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/30/08" by="David Hary" revision="1.00.3225.x">
''' Created
''' </history>
<System.Runtime.InteropServices.ComVisible(False)> _
Public Interface IUserLogOn

  ''' <summary>
  ''' Gets or sets a master password for log on override
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property MasterPassword() As String

  ''' <summary>
  ''' Validates a user name and password.
  ''' </summary>
  ''' <param name="userName">Specifies a user name.</param>
  ''' <param name="password">Specifies a password</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Validate(ByVal userName As String, ByVal password As String) As Boolean

  ''' <summary>
  ''' Validates a user name and password.
  ''' </summary>
  ''' <param name="userName">Specifies a user name.</param>
  ''' <param name="password">Specifies a password</param>
  ''' <param name="allowedUserRoles">Specifies the list of valid roles. This could be compared to the name of enumeration flags.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Validate(ByVal userName As String, ByVal password As String, ByVal allowedUserRoles As Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

  ''' <summary>
  ''' Validates a user name and role. Verifiy that the user has the required role.
  ''' </summary>
  ''' <param name="userName">Specifies a user name.</param>
  ''' <param name="allowedUserRoles">Specifies the list of valid roles. This could be compared to the name of enumeration flags.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Validate(ByVal userName As String, ByVal allowedUserRoles As Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

  ''' <summary>
  ''' Gets thevalidation message.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property ValidationMessage() As String

End Interface
