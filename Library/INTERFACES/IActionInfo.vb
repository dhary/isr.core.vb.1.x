﻿''' <summary>
''' The contract implemented by entites providing information about an action.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/15/2009" by="David Hary" revision="1.1.3422.x">
''' Created
''' </history>
Public Interface IActionInfo

  ''' <summary>
  ''' Returns the details of the last action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Details() As String

  ''' <summary>
  ''' Returns a hint about the last action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Hint() As String

  ''' <summary>Gets or sets the instance name.</summary>
  Property InstanceName() As String

  ''' <summary>
  ''' Returns a synopsis of the last action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Synopsis() As String

End Interface
