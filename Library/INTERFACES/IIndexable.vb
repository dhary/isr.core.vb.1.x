﻿''' <summary>
''' Defines an interface allowing objects to be indexed.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface IIndexable

  ''' <summary>
  ''' Gets or set the zero-based index.
  ''' A negative index indicates that the index were not set. 
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property Index() As Integer

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.Collection">collection</see> of
''' <see cref="IIndexable">Indexable</see> 
''' items index by the <see cref="IIndexable.Index">Index.</see>
''' </summary>
''' <remarks></remarks>
Public Class IndexableCollection(Of TItem As IIndexable)
  Inherits Collections.ObjectModel.Collection(Of TItem)

End Class

