﻿''' <summary>
''' The contract implemented by any class that can clear it contents.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Created
''' </history>
Public Interface IClearable

  ''' <summary>
  ''' Clear all class elements. 
  ''' For example, devices clear error queues and reset registers to zero.
  ''' </summary>
  Function Clear() As Boolean

End Interface

''' <summary>
''' The contract implemented by any class that can clear it contents.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Created
''' </history>
Public Interface IClearable(Of T As Structure)
  Inherits IClearable

  ''' <summary>
  ''' Gets or sets the value that is set when the structure is cleared.
  ''' </summary>
  Property ClearValue() As T?

End Interface
