﻿''' <summary>
''' Defines an interface for message queuing.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
''' Created
''' </history>
Public Interface IMessageQueuing

  ''' <summary>
  ''' Returns all last interface messages.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function DequeueMessages() As String

  ''' <summary>
  ''' Enqueues a message.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function EnqueueMessage(ByVal value As String) As String

  ''' <summary>
  ''' Enqueues a message.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="args"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function EnqueueFormat(ByVal format As String, ByVal ParamArray args() As Object) As String

  ''' <summary>
  ''' Clears the queue of interface Messages.
  ''' </summary>
  ''' <remarks></remarks>
  Sub ClearMessages()

  ''' <summary>
  ''' Returns true if messages are available.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function HasMessages() As Boolean

  ''' <summary>
  ''' Returns all interface messages without clearing the queue.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function ToString() As String

End Interface
