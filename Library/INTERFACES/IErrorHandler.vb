﻿''' <summary>
''' Defines an interface for raising error messages.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/06/2008" by="David Hary" revision="1.0.3627.x">
''' Created
''' </history>
Public Interface IErrorHandler

  ''' <summary>The event is raised when an error occurred.</summary>
  ''' <param name="e">A reference to the <see cref="T:System.Threading.ThreadExceptionEventArgs"/>event arguments</param>
  ''' <remarks>Must be declared the old way to work with COM.</remarks>
  Event ErrorAvailable As EventHandler(Of System.Threading.ThreadExceptionEventArgs)

  ''' <summary>Raises an error message.</summary>
  ''' <param name="ex">A reference to the <see cref="system.Exception"/>exception</param>
  Sub OnErrorAvailable(ByVal ex As Exception)

End Interface
