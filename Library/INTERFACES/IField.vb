﻿''' <summary>
''' Defines an interface for returning a string representation of a value.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/15/2009" by="David Hary" revision="1.1.3422.x">
''' Created
''' </history>
Public Interface IField

  ''' <summary>
  ''' Gets the caption.  This is the same as the field value.
  ''' </summary>
  ReadOnly Property Caption() As String

  ''' <summary>Gets or sets the format string for returning the field value.
  ''' </summary>
  Property Format() As String

  ''' <summary>Gets or sets the maximum length of the field value.
  ''' </summary>
  Property MaxLength() As Integer

  ''' <summary>Gets or sets the Minimum length of the field value.
  ''' </summary>
  Property MinLength() As Integer

  ''' <summary>
  ''' Returns the <see cref="Caption">field value</see>.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function ToString() As String

  ''' <summary>
  ''' Validates the field value for length.
  ''' </summary>
  Function ValidateFieldValue() As Boolean

End Interface
