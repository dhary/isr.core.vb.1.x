﻿''' <summary>
''' Provides an interface for elements that require presetting to a known state.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/07/2009" by="David Hary" revision="1.1.3414.x">
''' Created
''' </history>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Rename method to Preset as using Preset known state applies to devices and could be modified therein.
''' </history>
Public Interface IPresettable

  ''' <summary>Returns subsystem to its preset values.</summary>
  Function Preset() As Boolean

End Interface

''' <summary>
''' Provides an interface for elements that require presetting to a known state.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/07/2009" by="David Hary" revision="1.1.3414.x">
''' Created
''' </history>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Rename method to Preset as using Preset known state applies to devices and could be modified therein.
''' </history>
Public Interface IPresettable(Of T As Structure)

  Inherits IPresettable

  ''' <summary>
  ''' Gets or sets the value that is set when the structure is preset.
  ''' </summary>
  Property PresetValue() As T?

End Interface
