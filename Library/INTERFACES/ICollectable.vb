﻿''' <summary>
''' The contract implemented by objects to be included in a Dictionary collection of String and Value.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface ICollectable

  ''' <summary>Gets or sets the unique id of the instance of this class.</summary>
  ''' <value><c>UniqueId</c> is an <see cref="System.Integer">Integer</see> property.</value>
  ''' <remarks>Identifies the object in a collection of objects. Using <see cref="UniqueKey"/> 
  '''   permits setting a unique string for the unique id.</remarks>
  Property UniqueId() As Integer

  ''' <summary>Gets the unique Key for an instance of this class based on the
  ''' <see cref="UniqueId">unique ID</see>.</summary>
  ''' <value><c>UniqueKey</c> is a <see cref="System.String">String</see> property.</value>
  Property UniqueKey() As String

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="ICollectable">collectable</see> 
''' items keyed by the <see cref="ICollectable.UniqueKey">unique key.</see>
''' </summary>
''' <remarks></remarks>
Public Class CollectableCollection(Of TItem As ICollectable)
  Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

  Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
    Return item.UniqueKey
  End Function

End Class

