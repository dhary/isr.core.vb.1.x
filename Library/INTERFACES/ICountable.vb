﻿''' <summary>
''' Defines an interface allowing objects to be counted.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface ICountable

  ''' <summary>
  ''' Gets or set the serial number of this item.  This would typically be 
  ''' the order of this item in the list.
  ''' The serial number is one based.  A serial number of zero indicates that the
  ''' serial number was not set. 
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property SerialNumber() As Integer

End Interface

