﻿''' <summary>
''' Interface for providing connection information for a database.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/14/2009" by="David Hary" revision="1.2.3605.x">
''' Created
''' </history>
Public Interface IConnectionInfo

  ''' <summary>
  ''' Raised when the connection string is updated. 
  ''' </summary>
  ''' <remarks></remarks>
  Event ConnectionStringChanged As EventHandler(Of System.EventArgs)

  ''' <summary>
  ''' Gets or sets the connection string.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property ConnectionString() As String

  ''' <summary>
  ''' Gets or sets the database file information.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property DatabaseFileInfo() As System.IO.FileInfo

  ''' <summary>
  ''' Gets or sets the the database name.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property DatabaseName() As String

  ''' <summary>
  ''' Gets or sets the SQL compatibility level for this entity. 
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property DataSourceCompatibilityLevel() As Integer

  ''' <summary>
  ''' Gets or sets the database server instance name.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property InstanceName() As String

  ''' <summary>
  ''' Gets or sets the database server name.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property ServerName() As String

End Interface
