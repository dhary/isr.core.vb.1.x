﻿''' <summary>
''' Provides an interface for elements that require initializing of internal componenents.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/07/2008" by="David Hary" revision="1.0.2926.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface IInitialize

  ''' <summary>
  ''' Initializes internal elements.  Is called by <see cref="InitializeComponent">initialize</see>
  ''' as well as the constuctor. Must be private.
  ''' </summary>
  ''' <returns>True if initialized.</returns>
  ''' <remarks></remarks>
  Function InitializeComponent() As Boolean

End Interface
