﻿''' <summary>
''' The contract implemented by numeric val;ue that could approximated.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface IApproximateable

  ''' <summary>
  ''' Gets or sets the minimum noticable difference between the values.
  ''' This is the tolerance ot resulution of the values.
  ''' </summary>
  ''' <value>
  ''' The epsilon.
  ''' </value>
  ReadOnly Property Epsilon() As Object

End Interface
