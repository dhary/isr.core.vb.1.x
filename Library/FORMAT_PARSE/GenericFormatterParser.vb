﻿''' <summary>
''' Defines a generic formatter and parser element.
''' This parser is useful for getter and setter structrures such as in instrument interfaces.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Structure FormatterParser(Of T As {Structure, IFormattable})
  Implements IFormatterParser(Of T)

  ''' <summary>
  ''' Initializes a new instance of the <see cref="FormatterParser" /> struct.
  ''' </summary>
  ''' <param name="format">The format string.</param>
  Public Sub New(ByVal format As String)
    _format = format
  End Sub

#Region " EQUALS "

  ''' <summary>Determines if the two specified <see cref="FormatterParser(Of T)">cached comparable</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="Format">format</see> values.
  ''' </remarks>
  Public Overloads Function Equals(ByVal left As FormatterParser(Of T), ByVal right As FormatterParser(Of T)) As Boolean
    Return left.Format.Equals(right.Format)
  End Function

  ''' <summary>Indicates whether the current <see cref="T:FormatterParser(Of T)"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:FormatterParser(Of T)"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="obj">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overrides Function Equals(ByVal obj As Object) As Boolean
    Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse _
                                      (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, FormatterParser(Of T)))))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:FormatterParser(Of T)"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:FormatterParser(Of T)"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">A value.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As FormatterParser(Of T)) As Boolean
    Return Object.ReferenceEquals(Me, value) OrElse Me.Equals(Me, value)
  End Function

  ''' <summary>Returns True if the specified <see cref="T:BooleanElement"></see> value
  ''' equals the <paramref name="left">value</paramref>.</summary>
  ''' <returns>.</returns>
  ''' <param name="left">A <see cref="T:BooleanElement"></see> value.</param>
  ''' <param name="right">A <see cref="T:BooleanElement"></see> value.</param>
  Public Overloads Shared Operator =(ByVal left As FormatterParser(Of T), ByVal right As FormatterParser(Of T)) As Boolean
    Return right.Equals(left)
  End Operator

  ''' <summary>Returns True if the specified <see cref="T:BooleanElement"></see> value
  ''' is not equal the <paramref name="left">value</paramref>.</summary>
  ''' <returns>.</returns>
  ''' <param name="left">A <see cref="T:BooleanElement"></see> value.</param>
  ''' <param name="right">A <see cref="T:BooleanElement"></see> value.</param>
  Public Overloads Shared Operator <>(ByVal left As FormatterParser(Of T), ByVal right As FormatterParser(Of T)) As Boolean
    Return Not right.Equals(left)
  End Operator

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overrides Function GetHashCode() As Integer
    Dim hashCode As Integer
    If Not String.IsNullOrEmpty(_format) Then
      hashCode = hashCode Xor _format.GetHashCode
    End If
    Return hashCode
  End Function

#End Region

  Private _format As String
  ''' <summary>
  ''' Gets the format string.
  ''' </summary>
  ''' 
  Public ReadOnly Property Format() As String
    Get
      Return _format
    End Get
  End Property

  ''' <summary>
  ''' Returns the text representation of the boolean value
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Text(ByVal value As T) As String Implements IFormatterParser(Of T).Text
    If String.IsNullOrEmpty(Me.Format) Then
      Return value.ToString(Nothing, Globalization.CultureInfo.CurrentCulture)
    Else
      Return value.ToString(Me.Format, Globalization.CultureInfo.CurrentCulture)
    End If
  End Function

  ''' <summary>
  ''' Parses the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' </returns>
  Public Function Parse(ByVal value As String) As T? Implements IFormatterParser(Of T).Parse
    Dim result As T
    If StringExtensions.TryParse(Of T)(value, result) Then
      Return result
    Else
      Return New T?
    End If
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value can be parsed.
  ''' </returns>
  Public Function TryParse(ByVal value As String) As Boolean Implements IFormatterParser(Of T).TryParse
    Return Parse(value).HasValue
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <param name="result">The parsed value.</param>
  ''' <returns>
  ''' <c>true</c> if the value was parsed.
  ''' </returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#", Justification:="This is the standard call format for this method in Visual Studio")> _
  Public Function TryParse(ByVal value As String, ByRef result As T) As Boolean Implements IFormatterParser(Of T).TryParse
    Return StringExtensions.TryParse(Of T)(value, result)
  End Function

End Structure

