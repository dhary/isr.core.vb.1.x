﻿''' <summary>
''' Defines the contract for publihng failures.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="3.0.4038.x">
''' Created
''' </history>
Public Interface IFailurePublisher

#Region " I FAILURE PUBLISHER "

  ''' <summary>
  ''' Occurs when the instrument failed.
  ''' </summary>
  Event FailureOccurred As EventHandler(Of System.EventArgs)

  ''' <summary>
  ''' Raises the <see cref="E:FailureOccurred" /> event.
  ''' </summary>
  ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
  Sub OnFailureOccurred(ByVal e As System.EventArgs)

#End Region

End Interface
