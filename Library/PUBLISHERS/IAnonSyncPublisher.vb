﻿''' <summary>
''' The contract implemented by publishes to anon sync observers.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/02/2010" by="David Hary" revision="1.2.3988.x">
''' Created
''' </history>
Public Interface IAnonSyncPublisher

  Inherits System.ComponentModel.INotifyPropertyChanged, IDisposable

  ''' <summary>
  ''' Gets or sets the publishable status of the publisher. 
  ''' When created, the publisher is not publishable. 
  ''' This allows changing properties without affecting the observers.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property Publishable() As Boolean

  ''' <summary>
  ''' Raised to notify the binadable object of a change.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks>
  ''' Includes a work around because for some reason, the dinding wrte value does not occure.
  ''' This is dengerous becuase it could lead to a stack overflow.
  ''' It can be used if the property changed event is raised only if the property changed.
  ''' </remarks>
  Sub OnPropertyChanged(ByVal name As String)

  ''' <summary>
  ''' Publishes all values by raising the changed events.
  ''' </summary>
  ''' <remarks></remarks>
  Sub Publish()

  ''' <summary>
  ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
  ''' to use for marshaling events.
  ''' </summary>
  Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke

End Interface
