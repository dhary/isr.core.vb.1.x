﻿''' <summary>
''' A base class implementing an Anonymous Publisher.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public MustInherit Class AnonPublisher2Base

    Implements IAnonPublisher2

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>A private constructor for this class making it not publicly creatable.
    ''' This ensure using the class as a singleton.
    ''' </summary>
    Protected Sub New()
        MyBase.new()
        _isSuspended = True
        '_isValuesUpdated = False
        '_isUpdatedSinceLastContent = False
        '_isInitialized = False
        _info = New Core.MyStringBuilder
    End Sub

#End Region

#Region " CHANGES CONTROLS "

    Dim _info As Core.MyStringBuilder
    ''' <summary>
    ''' Returns reference to the published information.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable ReadOnly Property Info() As Core.MyStringBuilder
        Get
            Return _info
        End Get
    End Property

    Private _IsPublished As Boolean
    ''' <summary>Gets or the initializing sentinel. 
    ''' The publisher contents are deamed changeable only after they are first published.
    ''' This suspends all observer changes until the publisher status is deamed to be stable.
    ''' For instance, observers such as controls undergoing changes due, for example, to control
    ''' settings being set during the control initializations, can use this sentinel to suspend
    ''' changing the status of the publisher until this sentinel clears.
    ''' </summary>
    Public Overridable ReadOnly Property IsPublished() As Boolean Implements IAnonPublisher2.IsPublished
        Get
            Return _IsPublished
        End Get
    End Property

    Private _isUpdatedSinceLastContent As Boolean
    ''' <summary>
    ''' Gets the sentinel indicating if any value was updated since the last time the contents
    ''' was created.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property IsUpdatedSinceLastContent() As Boolean
        Get
            Return _isUpdatedSinceLastContent
        End Get
    End Property

    Private _isValuesUpdated As Boolean
    ''' <summary>
    ''' Gets the sentinel indicating if any value was updated since this proptery was last checked.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Property IsValuesUpdated() As Boolean Implements IAnonPublisher2.IsValuesUpdated
        Get
            If _isValuesUpdated Then
                _isValuesUpdated = False
                Return True
            Else
                Return False
            End If
        End Get
        Protected Set(ByVal value As Boolean)
            _isValuesUpdated = value
            _isUpdatedSinceLastContent = True
        End Set
    End Property

    Private _isSuspended As Boolean
    ''' <summary>
    ''' Gets the suspension status of the publisher. 
    ''' Suspension allows changing information without changing the observers.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property IsSuspended() As Boolean Implements IAnonPublisher2.IsSuspended
        Get
            Return _isSuspended
        End Get
    End Property

    ''' <summary>
    ''' Publishes all values by raising the changed events.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustOverride Sub Publish() Implements IAnonPublisher2.Publish

    ''' <summary>
    ''' Returns the contents of all published values.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Function PublishedContents() As Core.MyStringBuilder Implements IAnonPublisher2.PublishedContents
        If _isUpdatedSinceLastContent Then
            ' force a rebuild of the published info.
            _info = New Core.MyStringBuilder
        End If
        _isUpdatedSinceLastContent = False
        Return Info
    End Function

    ''' <summary>
    ''' Resumes real time publishing. Values are published whenever they change.
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub ResumePublishing() Implements IAnonPublisher2.ResumePublishing
        _isSuspended = False
    End Sub

    ''' <summary>
    ''' Suspends real time publishing. Values will be published only after publishing resumes or using
    ''' the <see cref="Publish">publish method</see>.
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Sub SuspendPublishing() Implements IAnonPublisher2.SuspendPublishing
        _isSuspended = True
    End Sub

#End Region

End Class
