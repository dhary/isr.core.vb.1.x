﻿''' <summary>
''' The contract implemented by publishes to anon observers.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/28/2010" by="David Hary" revision="1.2.3680.x">
''' Created
''' </history>
''' <history date="09/24/2011" by="David Hary" revision="1.2.4284.x">
''' Changes initialized to published.
''' </history>
Public Interface IAnonPublisher2

    ''' <summary>Gets or the initializing sentinel. 
    ''' The publisher contents are deamed changeable only after they are first published.
    ''' This suspends all observer changes until the publisher status is deamed to be stable.
    ''' For instance, observers such as controls undergoing changes due, for example, to control
    ''' settings being set during the control initializations, can use this sentinel to suspend
    ''' changing the status of the publisher until this sentinel clears.
    ''' </summary>
    ReadOnly Property IsPublished() As Boolean

    ''' <summary>
    ''' Gets the sentinel indicating if any value was updated since this proptery was last checked.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property IsValuesUpdated() As Boolean

    ''' <summary>
    ''' Gets the suspension status of the publisher. 
    ''' Suspension allows changing information without changing the observers.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property IsSuspended() As Boolean

    ''' <summary>
    ''' Publishes all values by raising the changed events.
    ''' </summary>
    ''' <remarks></remarks>
    Sub Publish()

    ''' <summary>
    ''' Returns the contents of all published values.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function PublishedContents() As isr.Core.MyStringBuilder

    ''' <summary>
    ''' Resumes real time publishing. Values are published whenever they change.
    ''' </summary>
    ''' <remarks></remarks>
    Sub ResumePublishing()

    ''' <summary>
    ''' Suspends real time publishing. Values will be published only after publishing resumes or using
    ''' the <see cref="Publish">publish method</see>.
    ''' </summary>
    ''' <remarks></remarks>
    Sub SuspendPublishing()

End Interface
